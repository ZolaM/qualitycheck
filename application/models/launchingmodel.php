<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Launchingmodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_launching');
		$query = $this->db->get('launching');
		return $query->row()->id_launching;
	}

	function ambillaunching($keyword, $bagian, $num=0, $rand=false) {		
		
		$this->db->distinct();
		$this->db->group_by('nama_komplau');
		$this->db->like('nama_komplau',$keyword);		
		$this->db->where('bagian_komplau', $bagian);
		
		$query = $this->db->get('komponen_launching');
		return $query->result();

	}

	function ambilitemlau($itemselected, $bagian) {
		$this->db->select('item_komplau');
		$this->db->select('tipe_datalau');
		$this->db->where('bagian_komplau', $bagian);
		$this->db->where('nama_komplau', $itemselected);
		$query = $this->db->get('komponen_launching');
		return $query;

	}

	public function tambahisilaunching($idproj, $idlau, $bagian, $namakomp, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_launching' => $idlau,
			'bagian_launching' => $bagian,			
			'nama_kompl' => $namakomp,			
			'id_kompl' => $idkomp,
			'prosla' => $pros,
			'alatla' => $alat,
			'rulela' => $rules,
			'qc_inspecl' => $qcinspec,
			'qa_coorl' => $qacoor,
			'class_surl' => $classsur,
			'owner_surl' => $ownersur,
			'state_regl' => $statereg,
			'tgl_periksal' => $tanggalperiksa,
			'status_launching' => $status,
			'tgl_reinspeksil' => $reinspek,
			'rekomendasi_lau' => $rekomendasi,
			'path_gambarl' => $path,
			'path_gambarl2' => $path2,
			'path_gambarl3' => $path3,
			'path_gambarl4' => $path4,
			'input_lau' => $wktinp,
			'oleh_lau' => $oleh
		);
		
		if($this->db->insert('launching', $data)){
			return true;		
		}
		else{
			return false;
		}
	}

	public function tambahitemlaunching($idlau, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_launching' => $idlau,
					'nama_iteml' => $item->nama,
					'isi_iteml' => $item->item,
					'standar_iteml' => $item->standard,
					'pihak_itemla' => $item->pihak
				);
				$this->db->insert('item_launching', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}


	function ambillistlaunching($bagian, $idproj) {	
		$this->db->where('id_project', $idproj);
		$this->db->where('bagian_launching', $bagian);
		$query = $this->db->get('launching');
		return $query;

	}

	function ambildetaillaunching($idproj, $idenmat, $bagian) {
		$this->db->where('id_project', $idproj);
		$this->db->where('id_launching', $idenmat);
		$this->db->where('bagian_launching', $bagian);
		$query = $this->db->get('launching');
		return $query;

	}


	function ambilulangitemlau($idlau) {		
		$this->db->where('id_launching', $idlau);
		$query = $this->db->get('item_launching');
		return $query;

	}



	public function suntingisilaunching($idproj, $idlau, $namakomp, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (														
			'nama_kompl' => $namakomp,			
			'id_kompl' => $idkomp,
			'prosla' => $pros,
			'alatla' => $alat,
			'rulela' => $rules,
			'qc_inspecl' => $qcinspec,
			'qa_coorl' => $qacoor,
			'class_surl' => $classsur,
			'owner_surl' => $ownersur,
			'state_regl' => $statereg,
			'tgl_periksal' => $tanggalperiksa,
			'status_launching' => $status,
			'tgl_reinspeksil' => $reinspek,
			'rekomendasi_lau' => $rekomendasi,
			'path_gambarl' => $path,
			'path_gambarl2' => $path2,
			'path_gambarl3' => $path3,
			'path_gambarl4' => $path4,
			'input_lau' => $wktinp,
			'oleh_lau' => $oleh
		);
		
		$this->db->where('id_project', $idproj);
		$this->db->where('id_launching', $idlau);
		if($this->db->update('launching', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function updateitemlaunching($idlau, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_iteml' => $item->item,
					'standar_iteml' => $item->standard,
					'pihak_itemla' => $item->pihak
				);
				$this->db->where('id_launching', $idlau);
				$this->db->where('nama_iteml', $item->nama);
				$this->db->update('item_launching', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function hapuslaunch($idlaunch){
		$this->db->where('id_launching', $idlaunch);
		$this->db->delete('launching');
		return $this->db->affected_rows();
	}
	
	//=============================== ITP ============================================
	public function submititplaunching($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'id_project' => $item->id_project,
					'id_komponen' => $item->id_komponen,
					'bagian' =>	$item->bagian,
					'nama_komponen' => $item->nama_komponen,
					'nama_item' =>$item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_launching', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}
	
	public function ambilITP($id){
		$this->db->select('*');
		$this->db->from('cl_delivery');
		$this->db->where('id_project',$id);
		$query = $this->db->get();
		return $query->result_array();	
	}
	
	public function ambilitemcllaunching($nama,$idproj){
		$this->db->where('id_project', $idproj);	
		$this->db->where('bagian', $nama);	
		$query = $this->db->get( 'cl_launching' );
		return $query;
	}
	
	public function ambilitemcllaunchingByKomponen($id){
		$this->db->where('id_komponen', $id);	
		$query = $this->db->get( 'cl_launching' );
		return $query;
	}
	
	public function editITPlaunching($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (						
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('id_komponen', $item->id_komponen);
				$this->db->where('nama_item', $item->nama_item);
				$this->db->where('id_project', $item->idproj);
				$this->db->update('cl_launching', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}




}