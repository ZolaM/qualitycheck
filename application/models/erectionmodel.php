<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Erectionmodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_erection');
		$query = $this->db->get('erection');
		return $query->row()->id_erection;
	}

	function ambilproseserection($num=0, $rand=false) {
		$sql = "SELECT DISTINCT proses_erection from komponen_erection";		

		if ($rand)
			$this->db->order_by( 'rand()' );
		if ($num != 0)
			$query = $this->db->query($sql);
		else
			$query = $this->db->query($sql);
		return $query;

	}

	function ambilitemerection($proses) {
		$this->db->select('item_erection');
		$this->db->select('tipe_dataer');
		$this->db->where('proses_erection', $proses);
		$query = $this->db->get('komponen_erection');
		return $query;

	}

	function ambilnamaassembly($keyword, $idproj, $num=0, $rand=false) {		
				
		$this->db->where('id_project', $idproj);
		$this->db->like('blok_assembly',$keyword);
		
		$query = $this->db->get('assembly');
		return $query->result();

	}

	function ceknamaas($nama) {
		
		$this->db->select('id_assembly');
		$this->db->where('blok_assembly', $nama);
		$query = $this->db->get('assembly');
		return $query;

	}

	function ambillisterection($idproj) {
		$this->db->where('id_project', $idproj);
		$query = $this->db->get('erection');
		return $query;

	}

	function ambildetailerection($idproj, $idenmat) {
		$this->db->where('id_project', $idproj);
		$this->db->where('id_erection', $idenmat);
		$query = $this->db->get('erection');
		return $query;

	}

	function ambilulangitemer($iderection) {		
		$this->db->where('id_erection', $iderection);
		$query = $this->db->get('item_erection');
		return $query;

	}






	public function tambahisierection($idproj, $id_erection, $proses, $namablok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_erection' => $id_erection,
			'proses_erection' => $proses,			
			'blok_erection' => $namablok,
			'proser' => $pros,
			'alater' => $alat,
			'ruleer' => $rules,
			'qc_inspecer' => $qcinspec,
			'qa_coorer' => $qacoor,
			'class_surrer' => $classsur,
			'owner_surer' => $ownersur,
			'state_reger' => $statereg,
			'tgl_periksaer' => $tanggalperiksa,
			'status_erection' => $status,
			'tgl_reinspeksier' => $reinspek,
			'rekomendasi_erection' => $rekomendasi,
			'path_gambarer' => $path,
			'path_gambarer2' => $path2,
			'path_gambarer3' => $path2,
			'path_gambarer4' => $path4,
			'input_er' => $wktinp,
			'oleh_er' => $oleh
		);
		
		if($this->db->insert('erection', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function tambahitemerection($id_erection, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_erection' => $id_erection,
					'nama_itemer' => $item->nama,
					'isi_itemer' => $item->item,
					'standard_itemer' => $item->standard,
					'pihak_itemer' => $item->pihak
				);
				$this->db->insert('item_erection', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}



	public function suntingisierection($idproj, $id_erection, $namablok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (					
			'blok_erection' => $namablok,	
			'proser' => $pros,
			'alater' => $alat,
			'ruleer' => $rules,		
			'qc_inspecer' => $qcinspec,
			'qa_coorer' => $qacoor,
			'class_surrer' => $classsur,
			'owner_surer' => $ownersur,
			'state_reger' => $statereg,
			'tgl_periksaer' => $tanggalperiksa,
			'status_erection' => $status,
			'tgl_reinspeksier' => $reinspek,
			'rekomendasi_erection' => $rekomendasi,
			'path_gambarer' => $path,
			'path_gambarer2' => $path2,
			'path_gambarer3' => $path2,
			'path_gambarer4' => $path4,
			'input_er' => $wktinp,
			'oleh_er' => $oleh
		);
		
		$this->db->where('id_project', $idproj);
		$this->db->where('id_erection', $id_erection);
		if($this->db->update('erection', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function updateitemerection($id_erection, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_itemer' => $item->item,
					'standard_itemer' => $item->standard,
					'pihak_itemer' => $item->pihak
				);
				$this->db->where('id_erection', $id_erection);
				$this->db->where('nama_itemer', $item->nama);
				$this->db->update('item_erection', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function hapuserec($iderec){
		$this->db->where('id_erection', $iderec);
		$this->db->delete('erection');
		return $this->db->affected_rows();
	}

	public function submititperec($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (		
					'id_project' => $item->idproj,
					'proses' => $item->proses,
					'nama_item' => $item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_erec', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function edititperec($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (		
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('id_project', $item->idproj);
				$this->db->where('proses', $item->proses);
				$this->db->where('nama_item', $item->nama_item);
					
				$this->db->update('cl_erec', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function ambilclerec($idproj){
		$this->db->where('id_project', $idproj);
		$query = $this->db->get('cl_erec');
		return $query;
	}

	public function ambilitemclerec($idproj, $proses){
		$this->db->where('id_project', $idproj);
		$this->db->where('proses', $proses);
		$query = $this->db->get('cl_erec');
		return $query;
	}







	//==================================================================================

	

	

	

	


	

	


	

	







	

	
}