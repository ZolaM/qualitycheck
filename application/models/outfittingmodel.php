<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Outfittingmodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_outfitting');
		$query = $this->db->get('outfitting');
		return $query->row()->id_outfitting;
	}

	function ambilout($keyword, $bagian, $num=0, $rand=false){
		$this->db->distinct();
		$this->db->group_by('nama_kompout');
		$this->db->like('nama_kompout',$keyword);		
		$this->db->where('bagian_kompout', $bagian);
		
		$query = $this->db->get('komponen_outfitting');
		return $query->result();
	}

	function ambiloutfitting($keyword, $bagian, $idproj, $num=0, $rand=false) {		
		
		$this->db->distinct();
		$this->db->group_by('nama_komp');
		$this->db->like('nama_komp',$keyword);		
		$this->db->where('bagian', $bagian);
		$this->db->where('id_project', $idproj);
		
		$query = $this->db->get('idenmaterial');
		return $query->result();

	}

	function ambilitemout($itemselected, $bagian) {
		$this->db->select('item_kompout');
		$this->db->select('tipe_dataout');
		$this->db->where('bagian_kompout', $bagian);
		$this->db->where('nama_kompout', $itemselected);
		$query = $this->db->get('komponen_outfitting');
		return $query;

	}



	public function tambahisioutfitting($idproj, $id_outfitting, $idnamaoutfitting, $bagian, $namaoutfitting, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_outfitting' => $id_outfitting,
			'idnamaout' => $idnamaoutfitting,
			'nama_bagianout' => $bagian,			
			'nama_outfitting' => $namaoutfitting,	
			'prosout' => $pros,
			'alatout' => $alat,
			'ruleout' => $rules,
			'qc_inspecout' => $qcinspec,
			'qa_coorout' => $qacoor,
			'class_surout' => $classsur,
			'owner_surout' => $ownersur,
			'state_regout' => $statereg,
			'tgl_periksaout' => $tanggalperiksa,
			'status_out' => $status,
			'tgl_reinspekout' => $reinspek,
			'rekomendasi_out' => $rekomendasi,
			'path_gambarout' => $path,
			'path_gambarout2' => $path2,
			'path_gambarout3' => $path3,
			'path_gambarout4' => $path4,
			'input_out' => $wktinp,
			'oleh_out' => $oleh
		);
		
		if($this->db->insert('outfitting', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function tambahitemoutfitting($id_outfitting, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_outfitting' => $id_outfitting,
					'nama_itemout' => $item->nama,
					'isi_itemout' => $item->item,
					'standard_itemout' => $item->standard,
					'pihak_itemout' => $item->pihak
				);
				$this->db->insert('item_outfitting', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	function ambillistoutfitting($bagian, $idproj) {	
		$this->db->where('id_project', $idproj);
		$this->db->where('nama_bagianout', $bagian);
		$query = $this->db->get('outfitting');
		return $query;

	}

	function ambildetailoutfitting($idproj, $idenmat, $bagian) {
		$this->db->where('id_project', $idproj);
		$this->db->where('id_outfitting', $idenmat);
		$this->db->where('nama_bagianout', $bagian);
		$query = $this->db->get('outfitting');
		return $query;

	}

	function ambilulangitemout($idout) {		
		$this->db->where('id_outfitting', $idout);
		$query = $this->db->get('item_outfitting');
		return $query;

	}


	public function suntingisioutfitting($idproj, $id_outfitting, $idnamaoutfitting, $namaoutfitting, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (									
			'nama_outfitting' => $namaoutfitting,
			'idnamaout' => $idnamaoutfitting,
			'prosout' => $pros,
			'alatout' => $alat,
			'ruleout' => $rules,			
			'qc_inspecout' => $qcinspec,
			'qa_coorout' => $qacoor,
			'class_surout' => $classsur,
			'owner_surout' => $ownersur,
			'state_regout' => $statereg,
			'tgl_periksaout' => $tanggalperiksa,
			'status_out' => $status,
			'tgl_reinspekout' => $reinspek,
			'rekomendasi_out' => $rekomendasi,
			'path_gambarout' => $path,
			'path_gambarout2' => $path2,
			'path_gambarout3' => $path3,
			'path_gambarout4' => $path4,
			'input_out' => $wktinp,
			'oleh_out' => $oleh
		);
		
		$this->db->where('id_project', $idproj);
		$this->db->where('id_outfitting', $id_outfitting);
		if($this->db->update('outfitting', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function updateitemoutfitting($id_outfitting, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_itemout' => $item->item,
					'standard_itemout' => $item->standard,
					'pihak_itemout' => $item->pihak
				);
				$this->db->where('id_outfitting', $id_outfitting);
				$this->db->where('nama_itemout', $item->nama);
				$this->db->update('item_outfitting', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function hapusoutf($idoutf){
		$this->db->where('id_outfitting', $idoutf);
		$this->db->delete('outfitting');
		return $this->db->affected_rows();
	}

	//================= ITP ===================
	public function submititpoutfitting($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'id_project' => $item->id_project,
					'id_komponen' => $item->id_komponen,
					'bagian' =>	$item->bagian,
					'nama_komponen' => $item->nama_komponen,
					'nama_item' =>$item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_outfitting', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}
	
	public function ambilITP($id){
		$this->db->select('*');
		$this->db->from('cl_outfitting');
		$this->db->where('id_project',$id);
		$query = $this->db->get();
		return $query->result_array();	
	}
	
	public function ambilitemcloutfitting($nama,$idproj){
		$this->db->where('id_project', $idproj);	
		$this->db->where('bagian', $nama);	
		$query = $this->db->get( 'cl_outfitting' );
		return $query;
	}
	
	public function ambilitemcloutfittingByKomponen($id){
		$this->db->where('id_komponen', $id);	
		$query = $this->db->get( 'cl_outfitting' );
		return $query;
	}
	
	public function editITPoutfitting($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (						
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('id_komponen', $item->id_komponen);
				$this->db->where('nama_item', $item->nama_item);
				$this->db->where('id_project', $item->idproj);
				$this->db->update('cl_outfitting', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}


}