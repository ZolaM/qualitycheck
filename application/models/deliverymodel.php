<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Deliverymodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_delivery');
		$query = $this->db->get('delivery');
		return $query->row()->id_delivery;
	}

	function ambildelivery($keyword) {		
		
		$this->db->distinct();
		$this->db->group_by('nama_dokumen');
		$this->db->like('nama_dokumen',$keyword);				
		
		$query = $this->db->get('komponen_delivery');
		return $query->result();

	}

	public function tambahisidelivery($idproj, $id_delivery, $namadok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_delivery' => $id_delivery,
			'nama_dokumen' => $namadok,	
			'prosdel' => $pros,
			'alatdel' => $alat,
			'ruledel' => $rules,
			'qc_inspecd' => $qcinspec,
			'qa_coord' => $qacoor,
			'class_surd' => $classsur,
			'owner_surd' => $ownersur,
			'state_regdel' => $statereg,
			'tgl_periksad' => $tanggalperiksa,
			'status_delivery' => $status,
			'tgl_reinspeksid' => $reinspek,
			'rekomendasi_delivery' => $rekomendasi,
			'path_gambard' => $path,
			'path_gambard2' => $path2,
			'path_gambard3' => $path3,
			'path_gambard4' => $path4,
			'input_del' => $wktinp,
			'oleh_del' => $oleh
		);
		
		if($this->db->insert('delivery', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function tambahitemdelivery($id_delivery, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_delivery' => $id_delivery,
					'nama_itemd' => $item->nama,
					'isi_itemd' => $item->item,
					'standard_itemd' => $item->standard,
					'pihak_itemdel' => $item->pihak
				);
				$this->db->insert('item_delivery', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}


	function ambillistdelivery($idproj) {	
		$this->db->where('id_project', $idproj);
		$query = $this->db->get('delivery');
		return $query;

	}

	function ambildetaildelivery($idproj, $idenmat){
		$this->db->where('id_project', $idproj);
		$this->db->where('id_delivery', $idenmat);		
		$query = $this->db->get('delivery');
		return $query;

	}

	function ambilulangitemdelivery($iddeliv) {		
		$this->db->where('id_delivery', $iddeliv);
		$query = $this->db->get('item_delivery');
		return $query;

	}

	public function updateisidelivery($idproj, $id_delivery, $namadok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (					
			'prosdel' => $pros,
			'alatdel' => $alat,
			'ruledel' => $rules,
			'qc_inspecd' => $qcinspec,
			'qa_coord' => $qacoor,
			'class_surd' => $classsur,
			'owner_surd' => $ownersur,
			'state_regdel' => $statereg,
			'tgl_periksad' => $tanggalperiksa,
			'status_delivery' => $status,
			'tgl_reinspeksid' => $reinspek,
			'rekomendasi_delivery' => $rekomendasi,
			'path_gambard' => $path,
			'path_gambard2' => $path2,
			'path_gambard3' => $path3,
			'path_gambard4' => $path4,
			'input_del' => $wktinp,
			'oleh_del' => $oleh
		);
		
		$this->db->where('id_project', $idproj);
		$this->db->where('id_delivery', $id_delivery);
		if($this->db->update('delivery', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function updateitemdelivery($id_delivery, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_itemd' => $item->item,
					'standard_itemd' => $item->standard,
					'pihak_itemdel' => $item->pihak
				);
				$this->db->where('id_delivery', $id_delivery);
				$this->db->where('nama_itemd', $item->nama);
				$this->db->update('item_delivery', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function hapusdel($iddel){
		$this->db->where('id_delivery', $iddel);
		$this->db->delete('delivery');
		return $this->db->affected_rows();
	}


















//====================================  ITP ========================================================
	public function submititpdelivery($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'id_project' => $item->id_project,
					'nama_dokumen' => $item->nama_dokumen,
					'nama_item' =>$item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_delivery', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}
	
	public function ambilITP($id){
		$this->db->select('*');
		$this->db->from('cl_delivery');
		$this->db->where('id_project',$id);
		$query = $this->db->get();
		return $query->result_array();	
	}
	
	public function ambilItemClDelivery($nama,$idproj){
		$this->db->where('id_project', $idproj);	
		$this->db->where('nama_dokumen', $nama);	
		$query = $this->db->get( 'cl_delivery' );
		return $query;
	}
	
	public function editITPDelivery($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (						
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('nama_dokumen', $item->nama_dokumen);
				$this->db->where('nama_item', $item->nama_item);
				$this->db->where('id_project', $item->idproj);
				$this->db->update('cl_delivery', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}


	

	

	

	


	

	












	//===========================================================================

	


	


	

	

	

	

	


	


	



}
