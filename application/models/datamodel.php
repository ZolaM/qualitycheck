<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Datamodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	//========================================== Ajax ===============================================================

	function remainder($idproj, $nilai, $tgl) {	
		// $this->db->where('id_project', $idproj);
		// $this->db->order_by($tgl, "asc");
		// $query = $this->db->get($nilai);
		$query = $this->db->query("SELECT * FROM ".$nilai." WHERE id_project=".$idproj." ORDER BY '".$tgl."'");
		return $query;

	}

	function logicsearch($idproj, $nilai, $keyword, $tabelkomp, $awal, $akhir, $tglperiksa) {
		if((empty($awal)) && (empty($akhir))){
			$this->db->like($tabelkomp , $keyword);
			$this->db->where('id_project', $idproj);
			$query = $this->db->get($nilai);	
		}
		else{
			//$query = ("SELECT * FROM ".$nilai." WHERE ".$tabelkomp." LIKE '%".$keyword."%' AND id_project=" .$idproj." AND (".$tglperiksa." BETWEEN '".$awal."' AND '".$akhir."')");
			$this->db->like($tabelkomp , $keyword);
			$this->db->where('id_project', $idproj);
			$this->db->where($tglperiksa.' >=', $awal);
			$this->db->where($tglperiksa.' <=', $akhir);
			$query = $this->db->get($nilai);

		}

		return $query;

	}

	function pemeriksaan($idproj, $nilai){
		$this->db->where('id_project', $idproj);
		$query = $this->db->get($nilai);

		return $query;
	}

	function ambillistpendaftar($nilai) {	
		$this->db->where('tipe_pengguna', $nilai);
		$query = $this->db->get('pendaftar');
		return $query;

	}

	function ambildetailpendaftar($id_pendaftar) {	
		$this->db->where('id_pendaftar', $id_pendaftar);
		$query = $this->db->get('pendaftar');
		return $query;

	}



	//=========================================== Submit Data =====================================================

	public function submitregowner($user, $pass, $nama, $telp, $namaperusahaan, $jabatan, $alamatpt, $tipekapal, $nopro, $pesan, $nik, $tipepengguna, $status){
		$data = array (
			'username_daftar' => $user,
			'password_daftar' => $pass,
			'nama_daftar' => $nama,			
			'no_telp' => $telp,			
			'jabatanposisi' => $namaperusahaan,
			'jabatan' => $jabatan,
			'alamat' => $alamatpt,
			'nik' => $nik,
			'tipe_kapal' => $tipekapal,
			'no_project' => $nopro,
			'infopesan' => $pesan,
			'tipe_pengguna' => $tipepengguna,
			'status' => $status			
		);
		
		if($this->db->insert('pendaftar', $data)){
			return true;		
		}
		else{
			return false;
		}
	}



	public function setujudaftar($id, $user, $password, $nama, $tipepengguna, $project){
		$ubahs = "SETUJU";
		$data2 = array (
			'status' => $ubahs,			
		);
		$this->db->where('id_pendaftar', $id);
		$this->db->update('pendaftar', $data2);


		$data = array (
			'username' => $user,
			'password' => $password,
			'nama_pengguna' => $nama,			
			'hak_akses' => $tipepengguna,			
			'hak_project' => $project,		
		);
		
		if($this->db->insert('pengguna', $data)){
			return true;		
		}
		else{
			return false;
		}
	}

	public function hapuspendaftar($idpendaftar){
		$this->db->where('id_pendaftar', $idpendaftar);
		$this->db->delete('pendaftar');
		return $this->db->affected_rows();
	}

	public function semuakomp($kwd){
		$query = $this->db->query("SELECT kompor
							FROM (
								SELECT nama_komp AS kompor
								FROM idenmaterial
								UNION 
								SELECT nama_kompfab AS kompor
								FROM fabrikasi
								UNION 
								SELECT blok_assembly AS kompor
								FROM assembly
								UNION 
								SELECT blok_erection AS kompor
								FROM erection
								UNION 
								SELECT nama_outfitting AS kompor
								FROM outfitting
								UNION 
								SELECT nama_kompl AS kompor
								FROM launching
								UNION 
								SELECT pengujian_comm AS kompor
								FROM commissioning
								UNION 
								SELECT pengujian_sea AS kompor
								FROM seatrial
								UNION 
								SELECT nama_dokumen AS kompor
								FROM delivery
							)hasil
							WHERE kompor LIKE '%".$kwd."%' ORDER BY kompor");
		return $query->result();
	}	

	function dataeval($idproj, $nilai, $tgl = null, $tgl2 = null, $tglperiksa){
		if(empty($tgl)){
			$this->db->where('id_project', $idproj);
			$query = $this->db->get($nilai);
		}
		else{
			$this->db->where('id_project', $idproj);
			$this->db->where($tglperiksa.' >=', $tgl);
			$this->db->where($tglperiksa.' <=', $tgl2);
			$query = $this->db->get($nilai);
		}
		return $query;
	}

	function kritsar($idproj){
		$this->db->select('*');
		$this->db->from('kritik_saran, pendaftar');
		$this->db->where('id_project', $idproj);
		$this->db->where('kritik_saran.username = pendaftar.username_daftar');
		$query = $this->db->get();

		return $query;
	}

	function saveKritSar($kritik, $saran){
		$data = array('id_project' => $this->session->userdata('pilihan_project'),
						'kritik' => $kritik,
						'saran' => $saran,
						'hak_akses' => $this->session->userdata('hak_akses'),
						'oleh' => $this->session->userdata('nama_pengguna'),
						'id_pengguna' => $this->session->userdata('id_pengguna'),
						'username' => $this->session->userdata('user'));
		$query = $this->db->insert('kritik_saran', $data);
		if($query){
			return 'sukses';
		}
		else 
			return 'gagal';
	}

	function ambildataks($idks){
		$this->db->where('id_kritsar', $idks);
		$query = $this->db->get('kritik_saran');

		return $query;
	}

	function updatekritsar($idks, $krit, $sar){
		$this->db->where('id_kritsar', $idks);
		$data = array('kritik' => $krit,
						'saran' => $sar);
		$this->db->update('kritik_saran', $data);
		$query = $this->db->affected_rows();
		return $query;

	}

	function hapuskritsar($idks){
		$this->db->where('id_kritsar', $idks);
		$this->db->delete('kritik_saran');
		$query = $this->db->affected_rows();
		return $query;

	}

	function fabpro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, proses_fabrikasi, status_fab
									FROM fabrikasi
									WHERE tgl_periksafab BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY proses_fabrikasi, status_fab
									ORDER BY proses_fabrikasi");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, proses_fabrikasi, status_fab
									FROM fabrikasi
									GROUP BY proses_fabrikasi, status_fab
									ORDER BY proses_fabrikasi');
		}

		return $query;
		
	}

	function asspro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, proses_assembly, status_assembly
									FROM assembly
									WHERE tgl_periksaass BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY proses_assembly, status_assembly
									ORDER BY proses_assembly");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, proses_assembly, status_assembly
									FROM assembly
									GROUP BY proses_assembly, status_assembly
									ORDER BY proses_assembly');
		}

		return $query;
		
	}

	function erpro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, proses_erection, status_erection
									FROM erection
									WHERE tgl_periksaer BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY proses_erection, status_erection
									ORDER BY proses_erection");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, proses_erection, status_erection
									FROM erection
									GROUP BY proses_erection, status_erection
									ORDER BY proses_erection');
		}

		return $query;
		
	}

	function idenpro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, bagian, status
									FROM idenmaterial
									WHERE tgl_periksa BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY bagian, status
									ORDER BY bagian");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, bagian, status
									FROM idenmaterial
									GROUP BY bagian, status
									ORDER BY bagian');
		}

		return $query;
		
	}

	function outpro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, nama_bagianout, status_out
									FROM outfitting
									WHERE tgl_periksaout BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY nama_bagianout, status_out
									ORDER BY nama_bagianout");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, nama_bagianout, status_out
									FROM outfitting
									GROUP BY nama_bagianout, status_out
									ORDER BY nama_bagianout');
		}

		return $query;
		
	}

	function laupro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, bagian_launching, status_launching
									FROM launching
									WHERE tgl_periksal BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY bagian_launching, status_launching
									ORDER BY bagian_launching");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, bagian_launching, status_launching
									FROM launching
									GROUP BY bagian_launching, status_launching
									ORDER BY bagian_launching');
		}

		return $query;
		
	}

	function compro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, bagian_comm, status_comm
									FROM commissioning
									WHERE tgl_periksaom BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY bagian_comm, status_comm
									ORDER BY bagian_comm");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, bagian_comm, status_comm
									FROM commissioning
									GROUP BY bagian_comm, status_comm
									ORDER BY bagian_comm');
		}

		return $query;
		
	}

	function seapro($tgl, $tgl2){
		if(!empty($tgl) && !empty($tgl2)){
			$query = $this->db->query("SELECT COUNT( id_project ) AS jml, bagian_sea, status_sea
									FROM seatrial
									WHERE tgl_periksaass BETWEEN '".$tgl."' AND '".$tgl2."' GROUP BY bagian_sea, status_sea
									ORDER BY bagian_sea");
		}
		else{
			$query = $this->db->query('SELECT COUNT( id_project ) AS jml, bagian_sea, status_sea
									FROM seatrial
									GROUP BY bagian_sea, status_sea
									ORDER BY bagian_sea');
		}

		return $query;
		
	}


}
