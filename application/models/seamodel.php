<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Seamodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_sea');
		$query = $this->db->get('seatrial');
		return $query->row()->id_sea;
	}

	function ambilsea($keyword, $bagian, $num=0, $rand=false) {		
		
		$this->db->distinct();
		$this->db->group_by('pengujian_sea');
		$this->db->like('pengujian_sea',$keyword);		
		$this->db->where('bagian_sea', $bagian);
		
		$query = $this->db->get('komponen_sea');
		return $query->result();

	}

	function ambilitemsea($itemselected, $bagian) {
		$this->db->select('item_sea');
		$this->db->select('tipe_datas');
		$this->db->where('bagian_sea', $bagian);
		$this->db->where('pengujian_sea', $itemselected);
		$query = $this->db->get('komponen_sea');
		return $query;

	}

	public function tambahisisea($idproj, $idsea, $bagian, $namauji, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_sea' => $idsea,
			'bagian_sea' => $bagian,			
			'pengujian_sea' => $namauji,			
			'id_komps' => $idkomp,
			'prossea' => $pros,
			'alatsea' => $alat,
			'rulesea' => $rules,
			'qc_inspecs' => $qcinspec,
			'qa_coors' => $qacoor,
			'class_surs' => $classsur,
			'owner_surs' => $ownersur,
			'state_regsea' => $statereg,
			'tgl_periksas' => $tanggalperiksa,
			'status_sea' => $status,
			'tgl_reinspeksis' => $reinspek,
			'rekomendasi_sea' => $rekomendasi,
			'path_gambars' => $path,
			'path_gambars2' => $path2,
			'path_gambars3' => $path3,
			'path_gambars4' => $path4,
			'input_sea' => $wktinp,
			'oleh_sea' => $oleh
		);
		
		if($this->db->insert('seatrial', $data)){
			return true;		
		}
		else{
			return false;
		}
	}

	public function tambahitemsea($idsea, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_sea' => $idsea,
					'nama_items' => $item->nama,
					'isi_items' => $item->item,
					'standard_items' => $item->standard,
					'pihak_itemsea' => $item->pihak
				);
				$this->db->insert('item_sea', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	function ambillistsea($bagian, $idproj) {	
		$this->db->where('id_project', $idproj);
		$this->db->where('bagian_sea', $bagian);
		$query = $this->db->get('seatrial');
		return $query;

	}

	function ambildetailsea($idproj, $idenmat){
		$this->db->where('id_project', $idproj);
		$this->db->where('id_sea', $idenmat);
		//$this->db->where('bagian_sea', $bagian);
		$query = $this->db->get('seatrial');
		return $query;

	}


	function ambilulangitemsea($idcomm) {		
		$this->db->where('id_sea', $idcomm);
		$query = $this->db->get('item_sea');
		return $query;

	}

	public function suntingisisea($idproj, $idsea, $namauji, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (														
			'pengujian_sea' => $namauji,			
			'id_komps' => $idkomp,
			'prossea' => $pros,
			'alatsea' => $alat,
			'rulesea' => $rules,
			'qc_inspecs' => $qcinspec,
			'qa_coors' => $qacoor,
			'class_surs' => $classsur,
			'owner_surs' => $ownersur,
			'state_regsea' => $statereg,
			'tgl_periksas' => $tanggalperiksa,
			'status_sea' => $status,
			'tgl_reinspeksis' => $reinspek,
			'rekomendasi_sea' => $rekomendasi,
			'path_gambars' => $path,
			'path_gambars2' => $path2,
			'path_gambars3' => $path3,
			'path_gambars4' => $path4,
			'input_sea' => $wktinp,
			'oleh_sea' => $oleh
		);
		
		$this->db->where('id_project', $idproj);
		$this->db->where('id_sea', $idsea);
		if($this->db->update('seatrial', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function suntingitemsea($idsea, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_items' => $item->item,
					'standard_items' => $item->standard,
					'pihak_itemsea' => $item->pihak
				);
				$this->db->where('nama_items', $item->nama);
				$this->db->where('id_sea', $idsea);
				$this->db->update('item_sea', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function hapussea($idsea){
		$this->db->where('id_sea', $idsea);
		$this->db->delete('seatrial');
		return $this->db->affected_rows();
	}












	//=============================== ITP ============================================
	public function submititpsea($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'id_project' => $item->id_project,
					'id_komponen' => $item->id_komponen,
					'bagian' =>	$item->bagian,
					'nama_komponen' => $item->nama_komponen,
					'nama_item' =>$item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_sea', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}
	
	public function ambilITP($id){
		$this->db->select('*');
		$this->db->from('cl_delivery');
		$this->db->where('id_project',$id);
		$query = $this->db->get();
		return $query->result_array();	
	}
	
	public function ambilitemclsea($nama,$idproj){
		$this->db->where('id_project', $idproj);	
		$this->db->where('bagian', $nama);	
		$query = $this->db->get( 'cl_sea' );
		return $query;
	}
	
	public function ambilitemclseaByKomponen($id){
		$this->db->where('id_komponen', $id);	
		$query = $this->db->get( 'cl_sea' );
		return $query;
	}
	
	public function editITPSea($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (						
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('id_komponen', $item->id_komponen);
				$this->db->where('nama_item', $item->nama_item);
				$this->db->where('id_project', $item->idproj);
				$this->db->update('cl_sea', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}
	


	


	

	

	

	

	


	


	



}
