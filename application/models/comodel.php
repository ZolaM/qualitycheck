<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Comodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_comm');
		$query = $this->db->get('commissioning');
		return $query->row()->id_comm;
	}

	function ambilcomm($keyword, $bagian, $num=0, $rand=false) {		
		
		$this->db->distinct();
		$this->db->group_by('pengujian_comm');
		$this->db->like('pengujian_comm',$keyword);		
		$this->db->where('bagian_comm', $bagian);
		
		$query = $this->db->get('komponen_comm');
		return $query->result();

	}


	function ambilitemcom($itemselected, $bagian) {
		$this->db->select('item_comm');
		$this->db->select('tipe_datacomm');
		$this->db->where('bagian_comm', $bagian);
		$this->db->where('pengujian_comm', $itemselected);
		$query = $this->db->get('komponen_comm');
		return $query;

	}


	public function tambahisicom($idproj, $idcom, $bagian, $namauji, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_comm' => $idcom,
			'bagian_comm' => $bagian,			
			'pengujian_comm' => $namauji,			
			'id_kompom' => $idkomp,
			'proscom' => $pros,
			'alatcom' => $alat,
			'rulecom' => $rules,
			'qc_inspecom' => $qcinspec,
			'qa_coorom' => $qacoor,
			'class_surom' => $classsur,
			'owner_surom' => $ownersur,
			'state_regcom' => $statereg,
			'tgl_periksaom' => $tanggalperiksa,
			'status_comm' => $status,
			'tgl_reinspeksiom' => $reinspek,
			'rekomendasi_comm' => $rekomendasi,
			'path_gambarom' => $path,
			'path_gambarom2' => $path2,
			'path_gambarom3' => $path3,
			'path_gambarom4' => $path4,
			'input_com' => $wktinp,
			'oleh_com' => $oleh
		);
		
		if($this->db->insert('commissioning', $data)){
			return true;		
		}
		else{
			return false;
		}
	}

	public function tambahitemcomm($idcom, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_comm' => $idcom,
					'nama_itemom' => $item->nama,
					'isi_itemom' => $item->item,
					'standard_itemom' => $item->standard,
					'pihak_itemom' => $item->pihak
				);
				$this->db->insert('item_comm', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	function ambillistcomm($bagian, $idproj) {	
		$this->db->where('id_project', $idproj);
		$this->db->where('bagian_comm', $bagian);
		$query = $this->db->get('commissioning');
		return $query;

	}

	function ambildetailcomm($idproj, $idenmat, $bagian) {
		$this->db->where('id_project', $idproj);
		$this->db->where('id_comm', $idenmat);
		$this->db->where('bagian_comm', $bagian);
		$query = $this->db->get('commissioning');
		return $query;

	}

	function ambilitemcomm($idcomm) {		
		$this->db->where('id_comm', $idcomm);
		$query = $this->db->get('item_comm');
		return $query;

	}


	public function suntingisicom($idproj, $idcom, $namauji, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4){
		$data = array (														
			'pengujian_comm' => $namauji,			
			'id_kompom' => $idkomp,
			'proscom' => $pros,
			'alatcom' => $alat,
			'rulecom' => $rules,
			'qc_inspecom' => $qcinspec,
			'qa_coorom' => $qacoor,
			'class_surom' => $classsur,
			'owner_surom' => $ownersur,
			'state_regcom' => $statereg,
			'tgl_periksaom' => $tanggalperiksa,
			'status_comm' => $status,
			'tgl_reinspeksiom' => $reinspek,
			'rekomendasi_comm' => $rekomendasi,
			'path_gambarom' => $path,
			'path_gambarom2' => $path2,
			'path_gambarom3' => $path3,
			'path_gambarom4' => $path4,
			'input_com' => $wktinp,
			'oleh_com' => $oleh
		);
		
		$this->db->where('id_project', $idproj);
		$this->db->where('id_comm', $idcom);
		if($this->db->update('commissioning', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function suntingitemcom($idcom, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_itemom' => $item->item,
					'standard_itemom' => $item->standard,
					'pihak_itemom' => $item->pihak
				);
				$this->db->where('id_comm', $idcom);
				$this->db->where('nama_itemom', $item->nama);
				$this->db->update('item_comm', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function hapuscomm($idcomm){
		$this->db->where('id_comm', $idcomm);
		$this->db->delete('commissioning');
		return $this->db->affected_rows();
	}

	//================= ITP ===================
	public function submititpcomm($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'id_project' => $item->id_project,
					'id_komponen' => $item->id_komponen,
					'bagian' =>	$item->bagian,
					'nama_komponen' => $item->nama_komponen,
					'nama_item' =>$item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_comm', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}
	
	public function ambilITP($id){
		$this->db->select('*');
		$this->db->from('cl_comm');
		$this->db->where('id_project',$id);
		$query = $this->db->get();
		return $query->result_array();	
	}
	
	public function ambilitemclcomm($nama,$idproj){
		$this->db->where('id_project', $idproj);	
		$this->db->where('bagian', $nama);	
		$query = $this->db->get( 'cl_comm' );
		return $query;
	}
	
	public function ambilitemclcommByKomponen($id){
		$this->db->where('id_komponen', $id);	
		$query = $this->db->get( 'cl_comm' );
		return $query;
	}
	
	public function editITPcomm($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (						
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('id_komponen', $item->id_komponen);
				$this->db->where('nama_item', $item->nama_item);
				$this->db->where('id_project', $item->idproj);
				$this->db->update('cl_comm', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}

}
