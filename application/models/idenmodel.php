<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Idenmodel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function ambilmaxid(){
		$this->db->select_max('id_idenmat');
		$query = $this->db->get('idenmaterial');
		return $query->row()->id_idenmat;
	}


	public function tambahidenhull($idproj, $idmat, $jenis, $namakomp, $namablok, $pros, $alat, $rules, $idkomp, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $bagian, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,
			'id_idenmat' => $idmat,
			'bagian' => $bagian,			
			'jenis_komp' => $jenis,
			'nama_komp' => $namakomp,
			'nama_blok' => $namablok,
			'prosiden' => $pros,
			'alatiden' => $alat,
			'ruleiden' => $rules,
			'id_komp' => $idkomp,
			'qc_inspec' => $qcinspec,
			'qa_coor' => $qacoor,
			'class_sur' => $classsur,
			'owner_sur' => $ownersur,
			'state_regiden' => $statereg,
			'tgl_periksa' => $tanggalperiksa,
			'status' => $status,
			'tgl_reinspeksi' => $reinspek,
			'rekomendasi' => $rekomendasi,
			'path_gambar' => $path,
			'path_gambar2' => $path2,
			'path_gambar3' => $path3,
			'path_gambar4' => $path4,
			'input_iden' => $wktinp,
			'oleh_iden' => $oleh
		);
		
		if($this->db->insert('idenmaterial', $data)){
			return true;		
		}
		else{
			return false;
		}
	}

	public function updatehullcon($idproj, $idmat, $namakomp, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh){
		$data = array (
			'id_project' => $idproj,						
			'nama_komp' => $namakomp,
			'id_komp' => $idkomp,
			'prosiden' => $pros,
			'alatiden' => $alat,
			'ruleiden' => $rules,
			'qc_inspec' => $qcinspec,
			'qa_coor' => $qacoor,
			'class_sur' => $classsur,
			'owner_sur' => $ownersur,
			'state_regiden' => $statereg,
			'tgl_periksa' => $tanggalperiksa,
			'status' => $status,
			'tgl_reinspeksi' => $reinspek,
			'rekomendasi' => $rekomendasi,
			'path_gambar' => $path,
			'path_gambar2' => $path2,
			'path_gambar3' => $path3,
			'path_gambar4' => $path4,
			'input_iden' => $wktinp,
			'oleh_iden' => $oleh
		);

		$this->db->where('id_project', $idproj);
		$this->db->where('id_idenmat', $idmat);
		if($this->db->update('idenmaterial', $data)){
			return true;		
		}
		else{
			return false;
		}
	}


	public function tambahitemhull($idmat, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (					
					'id_idenmat' => $idmat,
					'nama_item' => $item->nama,
					'isi_item' => $item->item,
					'standard_item' => $item->standard,
					'pihak_itemiden' => $item->pihak
				);
				$this->db->insert('item_idenmat', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}

	public function updateitemhull($idmat, $itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (										
					'isi_item' => $item->item,
					'standard_item' => $item->standard,
					'pihak_itemiden' => $item->pihak
				);
				$this->db->where('nama_item', $item->nama);
				$this->db->where('id_idenmat', $idmat);
				$this->db->update('item_idenmat', $data);
				
			}	
			return true;
		}
		else{
			return false;
		}
	}



	function ambillistiden($idproj, $nilai) {
	
		$this->db->where('id_project', $idproj);	
		$this->db->where('bagian', $nilai);	
		$query = $this->db->get( 'idenmaterial' );
		return $query;

	}


	function ambildetailiden($idproj, $nilai) {
	
		$this->db->where('id_project', $idproj);	
		$this->db->where('id_idenmat', $nilai);	
		$query = $this->db->get( 'idenmaterial' );
		return $query;

	}

	function ambilitemiden($nilai) {	
		$this->db->where('id_idenmat', $nilai);	
		$query = $this->db->get( 'item_idenmat' );
		return $query;

	}

	function ambilulangiden($idproj, $bagian, $idenparsing) {
	
		$this->db->where('id_project', $idproj);	
		$this->db->where('bagian', $bagian);	
		$this->db->where('id_idenmat', $idenparsing);	
		$query = $this->db->get( 'idenmaterial' );
		return $query;

	}

	function ambilkompmo($bagian, $keyword, $num=0, $rand=false) {		
		
		$this->db->where('bagian', $bagian);
		$this->db->like('nama_komp',$keyword);
		
		$query = $this->db->get('komponen_idenmat');
		return $query->result();

	}

	function ambilnamakomp($bagian, $keyword, $idproj, $num=0, $rand=false) {		
		
		$this->db->where('bagian', $bagian);
		$this->db->where('id_project', $idproj);
		$this->db->like('nama_komp',$keyword);
		
		$query = $this->db->get('idenmaterial');
		return $query->result();

	}

	function ceknamaiden($nama) {
		$this->db->select('id_idenmat');
		$this->db->where('nama_komp', $nama);	
		$query = $this->db->get( 'idenmaterial' );
		return $query;

	}
	
	public function hapusidenmat($id){
		$this->db->where('id_idenmat', $id);
		$this->db->delete('idenmaterial');
		return $this->db->affected_rows();
	}

	public function submititpiden($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (		
					'id_project' => $item->idproj,
					'bagian' => $item->bag,
					'jenis' => $item->jenis,
					'id_komp' => $item->idkomp,
					'nama_komp' => $item->nama_komp,
					'nama_item' => $item->nama_item,
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std
				);
				$this->db->insert('cl_iden', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}

	function edititpiden($itemlist){
		if (!empty($itemlist)) {
			foreach ($itemlist as $item) {
				$data = array (						
					'galangan' => $item->galangan,
					'owner' => $item->owner,
					'class' => $item->cls,
					'statereg' => $item->sr,
					'standard' => $item->std,
					'status' => $item->status
				);
				$this->db->where('id_project', $item->idproj);
				$this->db->where('bagian', $item->bag);
				$this->db->where('jenis', $item->jenis);
				$this->db->where('id_komp', $item->idkomp);
				$this->db->where('nama_komp', $item->nama_komp);
				$this->db->where('nama_item', $item->nama_item);
				$this->db->update('cl_iden', $data);
			}	
			return true;
		}
		else{
			return false;
		}
	}

	function ambilcliden($bag, $idproj) {	
		$this->db->where('bagian', $bag);	
		$this->db->where('id_project', $idproj);	
		$query = $this->db->get( 'cl_iden' );
		return $query;
	}

	function ambilitemcliden($idkomp, $idproj){
		$this->db->where('id_komp', $idkomp);	
		$this->db->where('id_project', $idproj);	
		$query = $this->db->get( 'cl_iden' );
		return $query;
	}
	
}