<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 1.4.1
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->

<!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/tables-data.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Aug 2015 01:26:21 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Reject</title>

    <meta name="description" content="data tables" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="#" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link href="<?php echo base_url(); ?>assets/css/beyond.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/demo.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="#" rel="stylesheet" type="text/css" />

    <!--Page Related styles-->
    <link href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.css" rel="stylesheet" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?php echo base_url(); ?>assets/js/skins.min.js"></script>
</head>
<!-- /Head -->
<!-- Body -->
<body>
    <!-- Loading Container -->
    <div class="loading-container">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="navbar-container">
                <!-- Navbar Barnd -->
                <div class="navbar-header pull-left">
                    <a href="#" class="navbar-brand">
                        <small>
                            <img src="<?php echo base_url(); ?>assets/img/logos.png" alt="" />
                        </small>
                    </a>
                </div>
                <!-- /Navbar Barnd -->
                <!-- Sidebar Collapse -->
                <div class="sidebar-collapse" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <!-- /Sidebar Collapse -->
                <!-- Account Area and Settings -->
                <div class="navbar-header pull-right">
                    <div class="navbar-account">
                        <ul class="account-area">
                            
                            <li>
                                <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                    <div class="avatar" title="View your public profile">
                                        <?php   
                                            if($this->session->userdata('hak_akses') == 1){
                                            ?>

                                                <img src="<?php echo base_url(); ?>assets/img/adminimg.png">
                                            <?php
                                            }
                                            else if ($this->session->userdata('hak_akses') == 2) {
                                            ?>
                                                <img src="<?php echo base_url(); ?>assets/img/ownerimg.png">
                                            <?php
                                            }
                                            else{
                                            ?>
                                                <img src="<?php echo base_url(); ?>assets/img/qaimg.png"> 
                                            <?php
                                            }
                                                          
                                        ?>
                                    </div>
                                    <section>
                                        <h2><span class="profile"><span><?php echo $this->session->userdata('nama_pengguna');?></span></span></h2>
                                    </section>
                                </a>
                                <!--Login Area Dropdown-->
                                <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                                    <li class="username"><a><?php echo $this->session->userdata('nama_pengguna');?></a></li>
                                    <!--/Theme Selector Area-->
                                    <li class="dropdown-footer">
                                        <a href="<?php echo base_url(); ?>login/do_logout">
                                            Sign out
                                        </a>
                                    </li>
                                </ul>
                                <!--/Login Area Dropdown-->
                            </li>
                            
                        </ul>

                        <!-- Settings -->
                    </div>
                </div>
                <!-- /Account Area and Settings -->
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?php   
                if($this->session->userdata('hak_akses') == 1){
                

                    $this->load->view('navbar/navbarlanjut');
                
                }
                else if ($this->session->userdata('hak_akses') == 2) {
                
                    $this->load->view('navbar/navbarall');
                
                }
                else{
                
                    $this->load->view('navbar/navbarqa');
                
                }
                              
            ?>
            
            
            <div class="page-content">
                <!-- Page Breadcrumb -->
                <div class="page-breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-anchor"></i>
                            <a href="#"><?php echo $this->session->userdata('nama_project')?></a>
                        </li>
                        
                        <li class="active">Reject</li>
                    </ul>
                </div>
                <!-- /Page Breadcrumb -->
                <!-- Page Header -->
                <div class="page-header position-relative">
                    <div class="header-title">
                        <h1>
                           Reject
                            
                        </h1>
                    </div>
                    <!--Header Buttons-->
                    <div class="header-buttons">
                        <a class="sidebar-toggler" href="#">
                            <i class="fa fa-arrows-h"></i>
                        </a>
                        <a class="refresh" id="refresh-toggler" href="#">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </a>
                        <a class="fullscreen" id="fullscreen-toggler" href="#">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                    </div>
                    <!--Header Buttons End-->
                </div>                
                <div class="page-body">
                    
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Reject</span>
                                    <div class="widget-buttons">
                                        <a href="#" data-toggle="maximize">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="#" data-toggle="collapse">
                                            <i class="fa fa-minus"></i>
                                        </a>
                                        <a href="#" data-toggle="dispose">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    
                                    <form id="registrationForm" action="" method="post" class="form-horizontal"
                                                  data-bv-message="This value is not valid"
                                                  data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                                                  data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                                                  data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
                                               <br>
                                               

                                                

                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Tahapan</label>
                                                    <div class="col-sm-8">
                                                        <select id="tahapan" name="tahapan" onchange="pilihan()">
                                                            <option value="null">--Silahkan Pilih--</option>
                                                            <option value="Semua">Semua Tahapan</option>
                                                            <option value="idenmaterial">Identifikasi Komponen</option>
                                                            <option value="fabrikasi">Fabrikasi</option>
                                                            <option value="assembly">Assembly</option>
                                                            <option value="erection">Erection</option>
                                                            <option value="outfitting">Outfitting</option>
                                                            <option value="launching">Launching</option>
                                                            <option value="commissioning">Commissioning</option>
                                                            <option value="seatrial">Sea Trial</option>
                                                            <option value="delivery">Delivery</option>
                                                        </select>                                                           
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Kualitas</label>
                                                    <label  class="col-sm-1 control-label"><strong id="tampilpersen"> 0 % </strong></label>
                                                    
                                                </div>
                                                <input id="idenparsing" type="hidden" name="idenparsing" />
                                                <input id="idproj" type="hidden" value="<?php echo $this->session->userdata('pilihan_project');?>" name="idproj" />


                                                
                                                

                                                <!-- ==========================identifikasi mat =========================== -->
                                                <div id="idenmat">
                                                    <!-- <br>
                                                    <br>
                                                    <div class="form-title" align="center">
                                                        List Identifikasi Komponen
                                                    </div>
                                                    <br>

                                                    <div class="form-group">  
                                                        <label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label>
                                                        <div class="col-lg-10">
                                                            <table class="table table-hover">
                                                                <thead class="bordered-darkorange">
                                                                    <tr>
                                                                       
                                                                        <th>
                                                                            Nama Komponen
                                                                        </th>
                                                                        <th>
                                                                            QC Inspector
                                                                        </th>
                                                                        <th>
                                                                            QA Coordinator
                                                                        </th>
                                                                        <th>
                                                                            Class Surveyor
                                                                        </th>
                                                                        <th>
                                                                            Owner Surveyor
                                                                        </th>
                                                                        <th>
                                                                            Status
                                                                        </th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="daftardetail">
                                                                     <tr>
                                                                        <td>Project 1234</td>
                                                                        <td>Alex Nilson</td>
                                                                        <td>100 m </td>
                                                                        <td>30 m</td>
                                                                        <td>10 m</td>
                                                                        <td>300 ton</td>
                                                                        
                                                                    </tr>                                                       
                                                                </tbody>                                                           
                                                            </table>

                                                        </div>
                                                    </div> -->
                                                </div>

                                                <!-- =========================== Fabrikasi ========================= -->
                                                <div id="fabrikasi">
                                                    
                                                </div>

                                                <!-- ============================ Assembly ======================= -->
                                                <div id="assembly">
                                                    
                                                </div>

                                                <!-- ============================ Erection ==================== -->
                                                <div id="erection">
                                                    
                                                </div>

                                                <!-- ============================= Outfitting ====================== -->
                                                <div id="outfitting">
                                                    
                                                </div>

                                                <!-- ============================ Launching ======================= -->
                                                <div id="launching">
                                                    
                                                </div>

                                                <!-- ============================== Commissioning ===================== -->
                                                <div id="commissioning">
                                                    
                                                </div>

                                                <!-- ============================== Sea Trial ======================= -->
                                                <div id="seatrial">
                                                    
                                                </div>

                                                <!-- ============================== Delivery ======================= -->
                                                <div id="delivery">
                                                    
                                                </div>

                                                
                                            </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Data Project</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table" id="isidetail">
                             
                            </table>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                
                </div>                
                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>

    <script type="text/javascript">

    function pilih(idproj, idenmat, link){
            document.getElementById('registrationForm').action = link;
            document.getElementById('idenparsing').value = idenmat;
            $('#form').submit();
        }

    var all;
    var oke;
    var reject;
    var tautan;

    function pilihan(){
        
        var nilai = document.getElementById('tahapan').value;
        var idproj = "<?php echo $this->session->userdata('pilihan_project'); ?>";
        all = 0;
        oke =0;
        reject = 0;
        document.getElementById('idenmat').innerHTML = '';
        document.getElementById('fabrikasi').innerHTML = '';
        document.getElementById('assembly').innerHTML = '';
        document.getElementById('erection').innerHTML = '';
        document.getElementById('outfitting').innerHTML = '';
        document.getElementById('launching').innerHTML = '';
        document.getElementById('commissioning').innerHTML = '';
        document.getElementById('seatrial').innerHTML = '';
        document.getElementById('delivery').innerHTML = '';
        
        if (nilai == "idenmaterial") {
            
            idenmat(idproj, nilai);
            cetak();
        }
        else if (nilai == "fabrikasi") {
            
            fabrikasi(idproj, nilai);
            cetak();
        }
        else if (nilai == "assembly") {
            assembly(idproj, nilai);
            cetak();
        }
        else if (nilai == "erection") {
            erection(idproj, nilai);
            cetak();
        }
        else if (nilai == "outfitting") {
            outfitting(idproj, nilai);
            cetak();
        }
        else if (nilai == "launching") {
            launching(idproj, nilai);
            cetak();
        }
        else if (nilai == "commissioning") {
            comm(idproj, nilai);
            cetak();
        }
        else if (nilai == "seatrial") {
            sea(idproj, nilai);
            cetak();
        }
        else if (nilai == "delivery") {
            delivery(idproj, nilai);
            cetak();
        }
        else if (nilai == "Semua"){
            var nilaiall = ['idenmaterial', 'fabrikasi', 'assembly', 'erection', 'outfitting', 'launching', 'commissioning', 'seatrial', 'delivery'];
            idenmat(idproj, nilaiall[0]);
            fabrikasi(idproj, nilaiall[1]);
            assembly(idproj, nilaiall[2]);
            erection(idproj, nilaiall[3]);
            outfitting(idproj, nilaiall[4]);
            launching(idproj, nilaiall[5]);
            comm(idproj, nilaiall[6]);
            sea(idproj, nilaiall[7]);
            delivery(idproj, nilaiall[8]);
            cetak();
        };

    }

    function idenmat(idproj, nilai){
        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, wkt:'tgl_periksa'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {

               var isiawal = '<br><br><div class="form-title" align="center">List Identifikasi Komponen</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Komponen</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail"></tbody></table></div></div>' ;

               document.getElementById('idenmat').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
              
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>idenmat/cetakiden";
                        var isidalam = '<tr><td>'+ data[i].nama_komp +'</td><td>'+ data[i].qc_inspec +'</td><td>'+ data[i].qa_coor +'</td><td>'+ data[i].class_sur +'</td><td>'+ data[i].owner_sur +'</td><td>'+ data[i].tgl_reinspeksi +'</td><td>'+ data[i].status +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailiden(\''+ idproj +'\', \''+ data[i].id_idenmat +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" type="submit" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_idenmat +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail').append(isidalam);
                    }
                                        
                }; 
                okes = data.length - countrej;
                reject = reject + countrej;
                oke = oke + okes;
                
            },
            async: false
            
        })
    }

    function lihatdetailiden(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailiden/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status == "OK") {

                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_komp+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komp+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspec+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coor+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_sur+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_sur+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regiden+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksa+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosiden+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatiden+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleiden+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambar2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_komp+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komp+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspec+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coor+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_sur+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_sur+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regiden+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksa+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksi+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosiden+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatiden+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleiden+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambar2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilitemiden/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_item +'</strong></td><td>'+ data[i].isi_item+'</td><td>'+ data[i].standard_item+'</td><td>'+ data[i].pihak_itemiden+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

    function fabrikasi(idproj, nilai){
        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksafab'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Fabrikasi</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Komponen</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail1"></tbody></table></div></div>' ;

               document.getElementById('fabrikasi').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_fab == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>fabrikasi/cetakfabrikasi";
                        var isidalam = '<tr><td>'+ data[i].nama_kompfab +'</td><td>'+ data[i].qc_inspecfab +'</td><td>'+ data[i].qa_coorfab +'</td><td>'+ data[i].class_surfab +'</td><td>'+ data[i].owner_surfab +'</td><td>'+ data[i].tgl_reinspeksifab +'</td><td>'+ data[i].status_fab +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailfab(\''+ idproj +'\', \''+ data[i].id_fabrikasi +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        '<button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_fabrikasi +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail1').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                oke = oke + okes;          
                
            },
            async: false
            
        })
    }

    function lihatdetailfab(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailfab/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status == "OK") {

                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_fabrikasi+'</td></tr><tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompfab+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompfab+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecfab+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorfab+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surfab+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surfab+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regfab+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksafab+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_fab+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosfab+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatfab+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulefab+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarfab2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_fabrikasi+'</td></tr><tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompfab+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompfab+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecfab+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorfab+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surfab+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surfab+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regfab+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksafab+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_fab+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksifab+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_fab +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosfab+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatfab+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulefab+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarfab2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilitemfab/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemfab +'</strong></td><td>'+ data[i].isi_itemfab+'</td><td>'+ data[i].standard_itemfab+'</td><td>'+ data[i].pihak_itemfab+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })                   
                    },
                })
        }

    function assembly(idproj, nilai){

        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksaass'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Assembly</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Blok</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th></th></tr></thead><tbody id="daftardetail2"></tbody></table></div></div>' ;

               document.getElementById('assembly').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_assembly == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>assembly/cetakassembly";
                        var isidalam = '<tr><td>'+ data[i].blok_assembly +'</td><td>'+ data[i].qc_inspecass +'</td><td>'+ data[i].qa_coorass +'</td><td>'+ data[i].class_surass +'</td><td>'+ data[i].owner_surass +'</td><td>'+ data[i].tgl_reinspeksiass +'</td><td>'+ data[i].status_assembly +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailass(\''+ idproj +'\', \''+ data[i].id_assembly +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_assembly +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail2').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetailass(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailassembly/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_assembly == "OK") {

                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_assembly+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_assembly+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecass+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorass+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surass+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surass+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regass+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaass+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_assembly+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosass+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatass+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleass+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarass2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_assembly+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_assembly+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecass+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorass+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surass+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surass+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regass+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaass+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_assembly+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksiass+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_assembly +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosass+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatass+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleass+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarass2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemass/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemas +'</strong></td><td>'+ data[i].isi_itemas+'</td><td>'+ data[i].standard_itemas+'</td><td>'+ data[i].pihak_itemas+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

    function erection(idproj, nilai){

        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksaer'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Erection</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Blok</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail3"></tbody></table></div></div>' ;

               document.getElementById('erection').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_erection == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>erection/cetakerection";
                        var isidalam = '<tr><td>'+ data[i].blok_erection +'</td><td>'+ data[i].qc_inspecer +'</td><td>'+ data[i].qa_coorer +'</td><td>'+ data[i].class_surer +'</td><td>'+ data[i].owner_surer +'</td><td>'+ data[i].tgl_reinspeksier +'</td><td>'+ data[i].status_erection +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailer(\''+ idproj +'\', \''+ data[i].id_erection +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_erection +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail3').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetailer(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailerection/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_erection == "OK") {

                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_erection+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_erection+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecer+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorer+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surrer+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surer+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_reger+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaer+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_erection+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proser+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alater+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleer+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarer2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_erection+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_erection+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecer+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorer+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surrer+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surer+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_reger+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaer+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_erection+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksier+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_erection +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proser+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alater+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleer+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarer2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemer/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemer +'</strong></td><td>'+ data[i].isi_itemer+'</td><td>'+ data[i].standard_itemer+'</td><td>'+ data[i].pihak_itemer+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })

                    },
                })
        }

    function outfitting(idproj, nilai){

        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksaout'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Outfitting</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Outfitting</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail4"></tbody></table></div></div>' ;

               document.getElementById('outfitting').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_out == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>outfitting/cetakoutfitting";
                        var isidalam = '<tr><td>'+ data[i].nama_outfitting +'</td><td>'+ data[i].qc_inspecout +'</td><td>'+ data[i].qa_coorout +'</td><td>'+ data[i].class_surout +'</td><td>'+ data[i].owner_surout +'</td><td>'+ data[i].tgl_reinspekout +'</td><td>'+ data[i].status_out +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailout(\''+ idproj +'\', \''+ data[i].id_outfitting +'\', \''+ data[i].nama_bagianout +'\')" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_outfitting +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail4').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetailout(idproj2, idenmat, bagian){
            //var bagian = document.getElementById('bagian').value;
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailoutfitting/',
                data: {idproj2:idproj2, idenmat:idenmat, bagian:bagian},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_out == "OK") {

                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].nama_bagianout+'</td></tr><tr><td><strong>Nama Outfitting</strong></td><td>'+ data[i].nama_outfitting+'</td></tr><tr><td><strong>ID Outfitting</strong></td><td>'+ data[i].idnamaout+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecout+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorout+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surout+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surout+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regout+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaout+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_out+'</td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarout2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].nama_bagianout+'</td></tr><tr><td><strong>Nama Outfitting</strong></td><td>'+ data[i].nama_outfitting+'</td></tr><tr><td><strong>ID Outfitting</strong></td><td>'+ data[i].idnamaout+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecout+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorout+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surout+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surout+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regout+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaout+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_out+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspekout+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_out +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosout+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatout+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleout+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarout2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemout/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemout +'</strong></td><td>'+ data[i].isi_itemout+'</td><td>'+ data[i].standard_itemout+'</td><td>'+ data[i].pihak_itemout+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

    function launching(idproj, nilai){

        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksal'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Launching</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Komponen</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail5"></tbody></table></div></div>' ;

               document.getElementById('launching').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_launching == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>launching/cetaklaunching";
                        var isidalam = '<tr><td>'+ data[i].nama_kompl +'</td><td>'+ data[i].qc_inspecl +'</td><td>'+ data[i].qa_coorl +'</td><td>'+ data[i].class_surl +'</td><td>'+ data[i].owner_surl +'</td><td>'+ data[i].tgl_reinspeksil +'</td><td>'+ data[i].status_launching +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailla(\''+ idproj +'\', \''+ data[i].id_launching +'\', \''+ data[i].bagian_launching +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_launching +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail5').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetailla(idproj2, idenmat, bagian){
            //var bagian = document.getElementById('bagian').value;
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetaillaunching/',
                data: {idproj2:idproj2, idenmat:idenmat, bagian:bagian},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_launching == "OK") {

                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompl+'</td></tr><tr><td><strong>ID Komponen</strong></td><td>'+ data[i].id_kompl+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecl+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorl+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surl+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surl+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regl+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksal+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_launching+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarl2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompl+'</td></tr><tr><td><strong>ID Komponen</strong></td><td>'+ data[i].id_kompl+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecl+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorl+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surl+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surl+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regl+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksal+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_launching+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksil+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_lau +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosla+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatla+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulela+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarl2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemlau/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_iteml +'</strong></td><td>'+ data[i].isi_iteml+'</td><td>'+ data[i].standar_iteml+'</td><td>'+ data[i].pihak_itemla+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })
                    },
                })
        }

    function comm(idproj, nilai){

        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksaom'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Commissioning</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Pengujian</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail6"></tbody></table></div></div>' ;

               document.getElementById('commissioning').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_comm == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>comm/cetakcomm";
                        var isidalam = '<tr><td>'+ data[i].pengujian_comm +'</td><td>'+ data[i].qc_inspecom +'</td><td>'+ data[i].qa_coorom +'</td><td>'+ data[i].class_surom +'</td><td>'+ data[i].owner_surom +'</td><td>'+ data[i].tgl_reinspeksiom +'</td><td>'+ data[i].status_comm +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailcom(\''+ idproj +'\', \''+ data[i].id_comm +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_comm +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail6').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetailcom(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailcomm/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_comm == "OK") {

                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_comm+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_comm+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompom+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecom+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorom+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surom+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surom+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regcom+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaom+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_comm+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarom2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_comm+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_comm+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompom+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecom+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorom+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surom+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surom+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regcom+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaom+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_comm+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksiom+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_comm +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proscom+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatcom+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulecom+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarom2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilitemcomm/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemom +'</strong></td><td>'+ data[i].isi_itemom+'</td><td>'+ data[i].standard_itemom+'</td><td>'+ data[i].pihak_itemom+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

    function sea(idproj, nilai){

        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksas'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Sea Trial</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Pengujian</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail7"></tbody></table></div></div>' ;

               document.getElementById('seatrial').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_sea == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>sea/cetaksea";
                        var isidalam = '<tr><td>'+ data[i].pengujian_sea +'</td><td>'+ data[i].qc_inspecs +'</td><td>'+ data[i].qa_coors +'</td><td>'+ data[i].class_surs +'</td><td>'+ data[i].owner_surs +'</td><td>'+ data[i].tgl_reinspeksis +'</td><td>'+ data[i].status_sea +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailsea(\''+ idproj +'\', \''+ data[i].id_sea +'\', \''+ data[i].bagian_sea +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_sea +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail7').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetailsea(idproj2, idenmat, bagian){
            //var bagian = document.getElementById('bagian').value;
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailsea/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_sea == "OK") {

                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_sea+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_sea+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komps+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecs+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coors+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surs+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surs+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regsea+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksas+' </td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambars2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_sea+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_sea+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komps+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecs+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coors+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surs+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surs+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regsea+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksas+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_sea+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksis+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_sea +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prossea+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatsea+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulesea+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambars2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemsea/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_items+'</strong></td><td>'+ data[i].isi_items+'</td><td>'+ data[i].standard_items+'</td><td>'+ data[i].pihak_itemsea+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

    function delivery(idproj, nilai){
        $.ajax({
            url: '<?php echo base_url(); ?>ajax/remainder/',
            data: {idproj:idproj, nilai:nilai, tgl:'tgl_periksad'},
            dataType: 'json',
            type: 'POST',
            success: function(data) {
               var isiawal = '<br><br><div class="form-title" align="center">List Delivery</div><br><div class="form-group"><label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label><div class="col-lg-10"><table class="table table-hover"><thead class="bordered-darkorange"><tr><th>Nama Dokumen</th><th>QC Inspector</th><th>QA Coordinator</th><th>Class Surveyor</th><th>Owner Surveyor</th><th>Tanggal Reinspeksi</th><th>Status</th><th>Action</th></tr></thead><tbody id="daftardetail8"></tbody></table></div></div>' ;

               document.getElementById('delivery').innerHTML = isiawal;
               var countrej = 0;
               all = all +  data.length;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].status_delivery == "Reject") {
                        countrej = countrej + 1;
                        tautan = "<?php echo base_url(); ?>delivery/cetakdelivery";
                        var isidalam = '<tr><td>'+ data[i].nama_dokumen +'</td><td>'+ data[i].qc_inspecd +'</td><td>'+ data[i].qa_coord +'</td><td>'+ data[i].class_surd +'</td><td>'+ data[i].owner_surd +'</td><td>'+ data[i].tgl_reinspeksid +'</td><td>'+ data[i].status_delivery +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetaildel(\''+ idproj +'\', \''+ data[i].id_delivery +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a>'+
                        ' <button class="btn btn-palegreen btn-xs" onclick="pilih(\''+ idproj +'\', \''+ data[i].id_delivery +'\', \''+tautan+'\')"><i class="fa fa-print"></i> Cetak Data</button></td></tr>';
                        $('#daftardetail8').append(isidalam);
                    }
                                        
                };     
                okes = data.length - countrej;
                reject = reject + countrej;
                
                oke = oke + okes;          
                
            },
            async: false
            
        })

    }

    function lihatdetaildel(idproj2, idenmat){            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetaildelivery/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_delivery == "OK") {

                            var isi2 = '<tr><td><strong>Nama Dokumen</strong></td><td>'+ data[i].nama_dokumen+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecd+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coord+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surd+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surd+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regdel+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksad+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_delivery+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambard2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Nama Dokumen</strong></td><td>'+ data[i].nama_dokumen+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecd+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coord+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surd+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surd+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regdel+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksad+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_delivery+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksid+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_delivery +' </td></tr><tr><td><strong>Gambar</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambard2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemdelivery/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemd +'</strong></td><td>'+ data[i].isi_itemd+'</td><td>'+ data[i].standard_itemd+'</td><td>'+ data[i].pihak_itemdel+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }


    function hitung(panjang, rej){

        all = all + panjang;
        oke = oke + (panjang - rej);
        reject = reject + rej;
        
    }

    function cetak(){
        var persentase = (oke / all)*100;
        document.getElementById('tampilpersen').innerHTML = persentase.toFixed(2) + ' %';
    }



        
    </script>

    <!--Basic Scripts-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="<?php echo base_url(); ?>assets/js/beyond.min.js"></script>

    <!--Google Analytics::Demo Only-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-60412744-1', 'auto');
        ga('send', 'pageview');

    </script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582Ltpw5OIinlRXCgcW0TV1adfWybesYCEodciL3JGIsv7en13ce4hFDZ37M9jzZlJAtRS%2bZpGll%2bIXCtnYeSKxh9QKzEStTlDeBEjiavsHM6neKgrAaVFV0pes88%2bEh4d4EC9Qqrr9ZD2FFxIfiUBpmA%2fs%2bCCg%2f83Zugd4Ci4jN884S92MT6sz47DuFCfm2JCsK8D1nCbJ5AblOm1O7EalTfGY7wSVDY%2fAmcNIRrEOmWuw7Dy7EfCH%2fd70jyHZwGVzbz5KpmvgwPPPAvX6sR5UHEPs%2bEylKB5Y0EXwxRm2ImHg4cPppZRPlDd8komIvHUvSWN0mNdlHCJe2KCKg9L6Rnhtacs0tvoplwsba24M%2bUxrB3NTZVo2lIsRLqCp42IrDz8NOsl%2fHJYVYcU0DUw7cDpQKIgR8jkUwPFUCHNtzPZU6EDwOQkbUow9%2f4g8su2muTM78Fit7qPj1f8j%2bDYtSDG9VH6jHxJTXx82A5D7bpw4MMfZ4fj45PcTVGBYnIRL6iMG4SbRKqXHBNdS7a1X%2fdOapMkRUHeeA%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
<!--  /Body -->

<!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/tables-data.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Aug 2015 01:26:29 GMT -->
</html>
