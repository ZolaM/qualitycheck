<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 1.4.1
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->

<!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/tables-data.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Aug 2015 01:26:21 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Search</title>

    <meta name="description" content="data tables" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="#" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link href="<?php echo base_url(); ?>assets/css/beyond.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/demo.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/typicons.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet" />
    <link id="skin-link" href="#" rel="stylesheet" type="text/css" />

    <!--Page Related styles-->
    <link href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="<?php echo base_url(); ?>assets/js/skins.min.js"></script>
</head>
<!-- /Head -->
<!-- Body -->
<body>
    <!-- Loading Container -->
    <div class="loading-container">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <div class="navbar">
        <div class="navbar-inner">
            <div class="navbar-container">
                <!-- Navbar Barnd -->
                <div class="navbar-header pull-left">
                    <a href="#" class="navbar-brand">
                        <small>
                            <img src="<?php echo base_url(); ?>assets/img/logos.png" alt="" />
                        </small>
                    </a>
                </div>
                <!-- /Navbar Barnd -->
                <!-- Sidebar Collapse -->
                <div class="sidebar-collapse" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <!-- /Sidebar Collapse -->
                <!-- Account Area and Settings -->
                <div class="navbar-header pull-right">
                    <div class="navbar-account">
                        <ul class="account-area">
                            
                            <li>
                                <a class="login-area dropdown-toggle" data-toggle="dropdown">
                                    <div class="avatar" title="View your public profile">
                                        <?php   
                                            if($this->session->userdata('hak_akses') == 1){
                                            ?>

                                                <img src="<?php echo base_url(); ?>assets/img/adminimg.png">
                                            <?php
                                            }
                                            else if ($this->session->userdata('hak_akses') == 2) {
                                            ?>
                                                <img src="<?php echo base_url(); ?>assets/img/ownerimg.png">
                                            <?php
                                            }
                                            else{
                                            ?>
                                                <img src="<?php echo base_url(); ?>assets/img/qaimg.png"> 
                                            <?php
                                            }
                                                          
                                        ?>
                                    </div>
                                    <section>
                                        <h2><span class="profile"><span><?php echo $this->session->userdata('nama_pengguna');?></span></span></h2>
                                    </section>
                                </a>
                                <!--Login Area Dropdown-->
                                <ul class="pull-right dropdown-menu dropdown-arrow dropdown-login-area">
                                    <li class="username"><a><?php echo $this->session->userdata('nama_pengguna');?></a></li>
                                    <!--/Theme Selector Area-->
                                    <li class="dropdown-footer">
                                        <a href="<?php echo base_url(); ?>login/do_logout">
                                            Sign out
                                        </a>
                                    </li>
                                </ul>
                                <!--/Login Area Dropdown-->
                            </li>
                            
                        </ul>

                        <!-- Settings -->
                    </div>
                </div>
                <!-- /Account Area and Settings -->
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?php   
                if($this->session->userdata('hak_akses') == 1){
                

                    $this->load->view('navbar/navbarlanjut');
                
                }
                else if ($this->session->userdata('hak_akses') == 2) {
                
                    $this->load->view('navbar/navbarall');
                
                }
                else{
                
                    $this->load->view('navbar/navbarqa');
                
                }
                              
            ?>
            
            <div class="page-content">
                <!-- Page Breadcrumb -->
                <div class="page-breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-anchor"></i>
                            <a href="#"><?php echo $this->session->userdata('nama_project')?></a>
                        </li>
                        
                        <li class="active">Search</li>
                    </ul>
                </div>
                <!-- /Page Breadcrumb -->
                <!-- Page Header -->
                <div class="page-header position-relative">
                    <div class="header-title">
                        <h1>
                           Search
                            
                        </h1>
                    </div>
                    <!--Header Buttons-->
                    <div class="header-buttons">
                        <a class="sidebar-toggler" href="#">
                            <i class="fa fa-arrows-h"></i>
                        </a>
                        <a class="refresh" id="refresh-toggler" href="#">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </a>
                        <a class="fullscreen" id="fullscreen-toggler" href="#">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                    </div>
                    <!--Header Buttons End-->
                </div>                
                <div class="page-body">
                    
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Search</span>
                                    <div class="widget-buttons">
                                        <a href="#" data-toggle="maximize">
                                            <i class="fa fa-expand"></i>
                                        </a>
                                        <a href="#" data-toggle="collapse">
                                            <i class="fa fa-minus"></i>
                                        </a>
                                        <a href="#" data-toggle="dispose">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    
                                    <form id="registrationForm" action="<?php echo base_url(); ?>outfitting/suntingoutfitting" method="post" class="form-horizontal"
                                                  data-bv-message="This value is not valid"
                                                  data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                                                  data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                                                  data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
                                               <br>
                                               

                                                

                                                <div class="form-group">
                                                    <label for="inputEmail3" class="col-sm-2 control-label no-padding-right">Nama Komponen, Blok, Dokumen, Pengujian</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="namakomp" name="namakomp" placeholder="Nama Komponen, Blok, Dokumen, Pengujian">                                                           
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="date" class="form-control" id="tglawal" name="tglawal" placeholder="">                                                           
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="date" class="form-control" id="tglakhir" name="tglakhir" placeholder="">                                                           
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="button" class="btn" id="buttoncek" name="buttoncek" value="Cek" onclick="carikomp()">                                                           
                                                    </div>
                                                </div>

                                               

                                                
                                                

                                                <!-- ==========================identifikasi mat =========================== -->
                                                <div id="idenmat">
                                                    <br>
                                                    <br>
                                                    <div class="form-title" align="center">
                                                        List Pencarian
                                                    </div>
                                                    <br>

                                                    <div class="form-group">  
                                                        <label for="inputEmail3" class="col-sm-1 control-label no-padding-right"></label>
                                                        <div class="col-lg-10">
                                                            <table class="table table-hover">
                                                                <thead class="bordered-darkorange">
                                                                    <tr>
                                                                       
                                                                        <th>
                                                                            Tahapan
                                                                        </th>
                                                                        <th>
                                                                            Nama Komponen
                                                                        </th>
                                                                        <th>
                                                                            Tanggal Periksa
                                                                        </th>
                                                                        <th>
                                                                            Status
                                                                        </th>
                                                                        <th>
                                                                            Action
                                                                        </th>
                                                                        
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="daftardetail">
                                                                     <!-- <tr>
                                                                        <td>Project 1234</td>
                                                                        <td>Alex Nilson</td>
                                                                        <td>100 m </td>
                                                                        <td>30 m</td>
                                                                       
                                                                        
                                                                    </tr>  -->                                                      
                                                                </tbody>                                                           
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>

                                                

                                                
                                            </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                    
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Data Project</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table" id="isidetail">
                             
                            </table>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                
                </div>                
                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>

    <script type="text/javascript">



    var tahapan = ['Identifikasi Komponen', 'Fabrikasi', 'Assembly', 'Erection', 'Outfitting', 'Launching', 'Commissioning', 'Sea Trial', 'Delivery'];
    var tahapancad = ['idenmaterial', 'fabrikasi', 'assembly', 'erection', 'outfitting', 'launching', 'commissioning', 'seatrial', 'delivery'];
    var namakomp = ['nama_komp', 'nama_kompfab', 'blok_assembly', 'blok_erection', 'nama_outfitting', 'nama_kompl', 'pengujian_comm', 'pengujian_sea', 'nama_dokumen'];
    var tglperiksa = ['tgl_periksa', 'tgl_periksafab', 'tgl_periksaass', 'tgl_periksaer', 'tgl_periksaout', 'tgl_periksal', 'tgl_periksaom', 'tgl_periksas', 'tgl_periksad'];

    

    function carikomp(){
        var keyword = document.getElementById('namakomp').value;
        var awal = document.getElementById('tglawal').value;
        var akhir = document.getElementById('tglakhir').value;
        var idproj = "<?php echo $this->session->userdata('pilihan_project'); ?>";
        document.getElementById('daftardetail').innerHTML = '';
        for (var i = 0; i < 9; i++) {
            nilai = tahapancad[i];
            tabelkomp = namakomp[i];   
            tperiksa = tglperiksa[i];

            $.ajax({
            url: '<?php echo base_url(); ?>ajax/logicsearch/',
            data: {idproj:idproj, keyword:keyword, nilai:nilai, tabelkomp:tabelkomp, awal:awal, akhir:akhir, tperiksa:tperiksa },
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                
                if (i==0) {
                    
                    for (var a = 0; a < data.length; a++) {
                        
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].nama_komp +'</td><td>'+ data[a].tgl_periksa +'</td><td>'+ data[a].status +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailiden(\''+ idproj +'\', \''+ data[a].id_idenmat +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>idenmat/suntingidentifikasii/'+idproj+'/'+data[a].id_idenmat+'/'+data[a].bagian+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };
                }
                else if(i==1){

                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].nama_kompfab +'</td><td>'+ data[a].tgl_periksafab +'</td><td>'+ data[a].status_fab +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailfab(\''+ idproj +'\', \''+ data[a].id_fabrikasi +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>fabrikasi/suntingfabrikasii/'+idproj+'/'+data[a].id_fabrikasi+'/'+data[a].bagian_fabrikasi+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };

                }
                else if(i==2){
                    
                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].blok_assembly +'</td><td>'+ data[a].tgl_periksaass +'</td><td>'+ data[a].status_assembly +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailass(\''+ idproj +'\', \''+ data[a].id_assembly +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>assembly/suntingassemblyy/'+idproj+'/'+data[a].id_assembly+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };

                }
                else if(i==3){
                    
                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].blok_erection +'</td><td>'+ data[a].tgl_periksaer +'</td><td>'+ data[a].status_erection +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailer(\''+ idproj +'\', \''+ data[a].id_erection +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>erection/suntingerectionn/'+idproj+'/'+data[a].id_erection+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</tr> ';
                        $('#daftardetail').append(isi);

                    };

                }
                else if(i==4){

                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].nama_outfitting +'</td><td>'+ data[a].tgl_periksaout +'</td><td>'+ data[a].status_out +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailout(\''+ idproj +'\', \''+ data[a].id_outfitting +'\', \''+ data[a].nama_bagianout +'\')" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>outfitting/suntingoutfittingg/'+idproj+'/'+data[a].id_outfitting+'/'+data[a].nama_bagianout+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };
                    
                }
                else if(i==5){

                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].nama_kompl +'</td><td>'+ data[a].tgl_periksal +'</td><td>'+ data[a].status_launching +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailla(\''+ idproj +'\', \''+ data[a].id_launching +'\', \''+ data[a].bagian_launching +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>launching/suntinglaunchingg/'+idproj+'/'+data[a].id_launching+'/'+data[a].bagian_launching+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };
                    
                }
                else if(i==6){

                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].pengujian_comm +'</td><td>'+ data[a].tgl_periksaom +'</td><td>'+ data[a].status_comm +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailcom(\''+ idproj +'\', \''+ data[a].id_comm +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>comm/suntingcomms/'+idproj+'/'+data[a].id_comm+'/'+data[a].bagian_comm+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };
                    
                }
                else if(i==7){

                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].pengujian_sea +'</td><td>'+ data[a].tgl_periksas +'</td><td>'+ data[a].status_sea +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetailsea(\''+ idproj +'\', \''+ data[a].id_sea +'\', \''+ data[a].bagian_sea +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>sea/suntingseaa/'+idproj+'/'+data[a].id_sea+'/'+data[a].bagian_sea+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };
                    
                }
                else if(i==8){
                    
                    for (var a = 0; a < data.length; a++) {
                    
                        var isi = '<tr><td>'+ tahapan[i] +'</td><td>'+ data[a].nama_dokumen +'</td><td>'+ data[a].tgl_periksad +'</td><td>'+ data[a].status_delivery +'</td>'+
                        '<td><a class="btn btn-info btn-xs " onclick="lihatdetaildel(\''+ idproj +'\', \''+ data[a].id_delivery +'\' )" data-toggle="modal" data-target="#myModal"><i class="fa fa-info"></i> Lihat Detail</a> <a href="<?php echo base_url();?>delivery/suntingdel/'+idproj+'/'+data[a].id_delivery+'" class="btn btn-palegreen btn-xs"><i class="fa fa-edit"></i> Sunting Data</a>'+
                        '</td></tr> ';
                        $('#daftardetail').append(isi);

                    };

                };              
                
            },
            async: false
            
            
            
        })

        }
    }

    function lihatdetailiden(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailiden/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status == "OK") {

                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_komp+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komp+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspec+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coor+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_sur+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_sur+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regiden+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksa+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosiden+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatiden+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleiden+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambar2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_komp+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komp+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspec+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coor+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_sur+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_sur+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regiden+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksa+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksi+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosiden+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatiden+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleiden+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambar2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambar4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambar4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilitemiden/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_item +'</strong></td><td>'+ data[i].isi_item+'</td><td>'+ data[i].standard_item+'</td><td>'+ data[i].pihak_itemiden+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetailfab(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailfab/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status == "OK") {

                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_fabrikasi+'</td></tr><tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompfab+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompfab+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecfab+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorfab+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surfab+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surfab+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regfab+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksafab+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_fab+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosfab+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatfab+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulefab+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarfab2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_fabrikasi+'</td></tr><tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompfab+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompfab+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecfab+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorfab+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surfab+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surfab+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regfab+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksafab+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_fab+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksifab+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_fab +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosfab+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatfab+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulefab+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarfab2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarfab4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarfab4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilitemfab/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemfab +'</strong></td><td>'+ data[i].isi_itemfab+'</td><td>'+ data[i].standard_itemfab+'</td><td>'+ data[i].pihak_itemfab+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

            $(document).ready(function () {
                    $(function () {
                        $("#namakomp").autocomplete({
                            source: function(request, response) {
                                $.ajax({ 
                                    url: "<?php echo base_url(); ?>ajax/semuakomponen/",
                                    data: { kode: $("#namakomp").val()},
                                    dataType: "json",
                                    type: "POST",
                                    success: function(data){
                                        response(data);

                                    }    
                                    
                                });
                            },
                            select: function(event, ui){
                                var itemselected = ui.item.value;
                               //alert(aa);
                            }


                        });
                    });
                });    

        function lihatdetailass(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailassembly/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_assembly == "OK") {

                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_assembly+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_assembly+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecass+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorass+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surass+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surass+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regass+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaass+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_assembly+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosass+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatass+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleass+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarass2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_assembly+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_assembly+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecass+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorass+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surass+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surass+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regass+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaass+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_assembly+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksiass+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_assembly +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosass+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatass+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleass+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarass2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarass4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarass4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemass/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemas +'</strong></td><td>'+ data[i].isi_itemas+'</td><td>'+ data[i].standard_itemas+'</td><td>'+ data[i].pihak_itemas+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetailer(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailerection/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_erection == "OK") {

                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_erection+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_erection+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecer+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorer+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surrer+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surer+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_reger+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaer+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_erection+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proser+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alater+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleer+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarer2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Proses</strong></td><td>'+ data[i].proses_erection+'</td></tr><tr><td><strong>Nama Blok</strong></td><td>'+ data[i].blok_erection+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecer+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorer+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surrer+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surer+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_reger+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaer+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_erection+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksier+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_erection +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proser+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alater+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleer+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarer2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarer4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarer4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemer/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemer +'</strong></td><td>'+ data[i].isi_itemer+'</td><td>'+ data[i].standard_itemer+'</td><td>'+ data[i].pihak_itemer+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetailout(idproj2, idenmat, bagian){
            //var bagian = document.getElementById('bagian').value;
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailoutfitting/',
                data: {idproj2:idproj2, idenmat:idenmat, bagian:bagian},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_out == "OK") {

                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].nama_bagianout+'</td></tr><tr><td><strong>Nama Outfitting</strong></td><td>'+ data[i].nama_outfitting+'</td></tr><tr><td><strong>ID Outfitting</strong></td><td>'+ data[i].idnamaout+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecout+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorout+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surout+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surout+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regout+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaout+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_out+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosout+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatout+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleout+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarout2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].nama_bagianout+'</td></tr><tr><td><strong>Nama Outfitting</strong></td><td>'+ data[i].nama_outfitting+'</td></tr><tr><td><strong>ID Outfitting</strong></td><td>'+ data[i].idnamaout+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecout+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorout+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surout+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surout+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regout+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaout+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_out+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspekout+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_out +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosout+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatout+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].ruleout+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarout2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarout4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarout4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemout/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemout +'</strong></td><td>'+ data[i].isi_itemout+'</td><td>'+ data[i].standard_itemout+'</td><td>'+ data[i].pihak_itemout+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetailla(idproj2, idenmat, bagian){
            //var bagian = document.getElementById('bagian').value;
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetaillaunching/',
                data: {idproj2:idproj2, idenmat:idenmat, bagian:bagian},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_launching == "OK") {

                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompl+'</td></tr><tr><td><strong>ID Komponen</strong></td><td>'+ data[i].id_kompl+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecl+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorl+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surl+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surl+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regl+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksal+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_launching+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosla+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatla+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulela+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarl2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Nama Komponen</strong></td><td>'+ data[i].nama_kompl+'</td></tr><tr><td><strong>ID Komponen</strong></td><td>'+ data[i].id_kompl+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecl+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorl+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surl+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surl+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regl+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksal+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_launching+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksil+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_lau +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prosla+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatla+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulela+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarl2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarl4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarl4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemlau/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_iteml +'</strong></td><td>'+ data[i].isi_iteml+'</td><td>'+ data[i].standar_iteml+'</td><td>'+ data[i].pihak_itemla+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetailcom(idproj2, idenmat){
            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailcomm/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_comm == "OK") {

                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_comm+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_comm+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompom+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecom+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorom+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surom+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surom+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regcom+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaom+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_comm+'</td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proscom+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatcom+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulecom+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarom2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_comm+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_comm+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_kompom+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecom+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coorom+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surom+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surom+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regcom+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksaom+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_comm+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksiom+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_comm +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].proscom+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatcom+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulecom+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambarom2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambarom4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambarom4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilitemcomm/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemom +'</strong></td><td>'+ data[i].isi_itemom+'</td><td>'+ data[i].standard_itemom+'</td><td>'+ data[i].pihak_itemom+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetailsea(idproj2, idenmat, bagian){
            //var bagian = document.getElementById('bagian').value;
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetailsea/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_sea == "OK") {

                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_sea+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_sea+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komps+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecs+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coors+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surs+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surs+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regsea+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksas+' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prossea+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatsea+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulesea+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambars2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Bagian</strong></td><td>'+ data[i].bagian_sea+'</td></tr><tr><td><strong>Nama Pengujian</strong></td><td>'+ data[i].pengujian_sea+'</td></tr><tr><td><strong>Id Komponen</strong></td><td>'+ data[i].id_komps+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecs+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coors+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surs+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surs+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regsea+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksas+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_sea+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksis+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_sea +' </td></tr><tr><td><strong>Prosedur Pemeriksaan</strong></td><td>'+data[i].prossea+'</td></tr><tr><td><strong>Alat Pemeriksaan</strong></td><td>'+data[i].alatsea+'</td></tr><tr><td><strong>Standard Rule</strong></td><td>'+data[i].rulesea+'</td></tr><tr><td><strong>Gambar 1</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambars2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambars4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambars4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemsea/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_items+'</strong></td><td>'+ data[i].isi_items+'</td><td>'+ data[i].standard_items+'</td><td>'+ data[i].pihak_itemsea+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        function lihatdetaildel(idproj2, idenmat){            
            $.ajax({
                url: '<?php echo base_url(); ?>ajax/ambildetaildelivery/',
                data: {idproj2:idproj2, idenmat:idenmat},
                dataType: 'json',
                type: 'POST',
                success: function(data) {
                    document.getElementById('isidetail').innerHTML = '';
                    for (var i = 0; i < data.length; i++) {
                       console.log(data[i]);
                        if (data[i].status_delivery == "OK") {

                            var isi2 = '<tr><td><strong>Nama Dokumen</strong></td><td>'+ data[i].nama_dokumen+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecd+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coord+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surd+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surd+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regdel+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksad+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_delivery+'</td></tr><tr><td><strong>Gambar</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambard2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard4+'" height="100"/></td></tr>');
                            }

                        }
                        else{
                            var isi2 = '<tr><td><strong>Nama Dokumen</strong></td><td>'+ data[i].nama_dokumen+'</td></tr><tr><td><strong>QC Inspector</strong></td><td>'+ data[i].qc_inspecd+'  </td></tr><tr><td><strong>QA Coordinator</strong></td><td>'+ data[i].qa_coord+'</td></tr><tr><td><strong>Class Surveyor</strong></td><td>'+ data[i].class_surd+' </td></tr><tr><td><strong>Owner Surveyor</strong></td><td>'+data[i].owner_surd+'</td></tr><tr><td><strong>State Regulator</strong></td><td>'+data[i].state_regdel+'</td></tr><tr><td><strong>Tanggal Periksa</strong></td><td>'+ data[i].tgl_periksad+' </td></tr><tr><td><strong>Status</strong></td><td>'+data[i].status_delivery+'</td></tr><tr><td><strong>Tanggal Reinspeksi</strong></td><td>'+ data[i].tgl_reinspeksid+'</td></tr><tr><td><strong>Rekomendasi</strong></td><td>'+ data[i].rekomendasi_delivery +' </td></tr><tr><td><strong>Gambar</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard+'" height="100"/></td></tr>';
                            $('#isidetail').append(isi2);

                            if(data[i].path_gambard2){
                                $('#isidetail').append('<tr><td><strong>Gambar 2</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard2+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard3){
                                $('#isidetail').append('<tr><td><strong>Gambar 3</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard3+'" height="100"/></td></tr>');
                            }

                            if(data[i].path_gambard4){
                                $('#isidetail').append('<tr><td><strong>Gambar 4</strong></td><td><img src="'+"<?php echo base_url()?>"+data[i].path_gambard4+'" height="100"/></td></tr>');
                            }
                        }
                        
                    };

                    var id_iden = idenmat;
                    $.ajax({
                        url: '<?php echo base_url(); ?>ajax/ambilulangitemdelivery/',
                        data: {id_iden:id_iden},
                        dataType: 'json',
                        type: 'POST',
                        success: function(data) {             
                            var pembatas = '<tr><td align="center" colspan="4"><strong>Item Pemeriksaan</strong></td></tr>'+
                                            '<tr><td><strong>Nama Item</strong></td><td><strong>Isi Item</strong></td><td><strong>Standard Item</strong></td><td><strong>Pemeriksa</strong></td></tr>';
                            $('#isidetail').append(pembatas);
                            for (var i = 0; i < data.length; i++) {
                                
                                var isi2 = '<tr><td><strong>'+ data[i].nama_itemd +'</strong></td><td>'+ data[i].isi_itemd+'</td><td>'+ data[i].standard_itemd+'</td><td>'+ data[i].pihak_itemdel+'</td></tr>';
                                $('#isidetail').append(isi2);
                            };

                            var pembatas2 = '<tr><td><strong></strong></td><td></td></tr>';
                            $('#isidetail').append(pembatas2);
                           
                            },
                        })




                   
                    },
                })
        }

        
    </script>

    <!--Basic Scripts-->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="<?php echo base_url(); ?>assets/js/beyond.min.js"></script>

    <!--Google Analytics::Demo Only-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-60412744-1', 'auto');
        ga('send', 'pageview');

    </script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request" + "?id=1" + "&enc=telkom2" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582Ltpw5OIinlRXCgcW0TV1adfWybesYCEodciL3JGIsv7en13ce4hFDZ37M9jzZlJAtRS%2bZpGll%2bIXCtnYeSKxh9QKzEStTlDeBEjiavsHM6neKgrAaVFV0pes88%2bEh4d4EC9Qqrr9ZD2FFxIfiUBpmA%2fs%2bCCg%2f83Zugd4Ci4jN884S92MT6sz47DuFCfm2JCsK8D1nCbJ5AblOm1O7EalTfGY7wSVDY%2fAmcNIRrEOmWuw7Dy7EfCH%2fd70jyHZwGVzbz5KpmvgwPPPAvX6sR5UHEPs%2bEylKB5Y0EXwxRm2ImHg4cPppZRPlDd8komIvHUvSWN0mNdlHCJe2KCKg9L6Rnhtacs0tvoplwsba24M%2bUxrB3NTZVo2lIsRLqCp42IrDz8NOsl%2fHJYVYcU0DUw7cDpQKIgR8jkUwPFUCHNtzPZU6EDwOQkbUow9%2f4g8su2muTM78Fit7qPj1f8j%2bDYtSDG9VH6jHxJTXx82A5D7bpw4MMfZ4fj45PcTVGBYnIRL6iMG4SbRKqXHBNdS7a1X%2fdOapMkRUHeeA%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
<!--  /Body -->

<!-- Mirrored from beyondadmin-v1.4.1.s3-website-us-east-1.amazonaws.com/tables-data.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 10 Aug 2015 01:26:29 GMT -->
</html>
