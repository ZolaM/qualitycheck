<div class="page-sidebar bg-blue" id="sidebar">
    <!-- Page Sidebar Header-->
    
    <!-- /Page Sidebar Header -->
    <!-- Sidebar Menu -->
    <ul class="nav sidebar-menu bg-blue">
        <!--Dashboard-->
        <li>
            <a href="<?php echo base_url(); ?>">
                <i class="menu-icon fa fa-anchor"></i>
                <span class="menu-text"> Back To Home </span>
            </a>
        </li>
        <!--Databoxes-->
        <li>

            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text"> Identifikasi Komponen</span>
                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatitpiden">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatiden">
                        <span class="menu-text">Lihat Ident Komp</span>
                    </a>
                </li> 
            </ul>

        </li>

        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Fabrikasi</span>
                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatitpfab">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatfabrikasi">
                        <span class="menu-text"> Lihat Fabrikasi </span>
                    </a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Assembly</span>
                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatitpass">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatassembly">
                        <span class="menu-text"> Lihat Assembly </span>
                    </a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Erection</span>
                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihatitperec">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>owner/lihaterection">
                        <span class="menu-text"> Lihat Erection </span>
                    </a>
                </li>
            </ul>
        </li>
        
       <li>
			<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Outfitting</span>
                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/LihatITPoutfittingUser">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
				<li>
					<a href="<?php echo base_url(); ?>owner/lihatoutfitting">
						<span class="menu-text"> Lihat Outfitting </span>
					</a>
				</li>
			</ul>
        </li>
        <li>
			<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Launching</span>
                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/LihatITPlaunchingUser">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
				<li>
					<a href="<?php echo base_url(); ?>owner/lihatlaunching">
						<span class="menu-text"> Lihat Launching </span>
					</a>
				</li>
			</ul>
        </li>
        <li>
			<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Commissioning</span>
                <i class="menu-expand"></i>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/LihatITPcommUser">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
				<li>
					<a href="<?php echo base_url(); ?>owner/lihatcommissioning">
						<span class="menu-text"> Lihat Commissioning </span>
					</a>
				</li>
			</ul>
        </li>
        <li>
			<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Sea Trial</span>
                <i class="menu-expand"></i>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/LihatITPseaUser">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
				<li>
					<a href="<?php echo base_url(); ?>owner/lihatseatrial">
						<span class="menu-text"> Lihat Sea Trial </span>
					</a>
				</li>
			</ul>
        </li>
        <li>
			<a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text">Delivery</span>
                <i class="menu-expand"></i>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url(); ?>owner/LihatITPdeliveryUser">
                        <span class="menu-text">Lihat ITP</span>
                    </a>
                </li>
				<li>
					<a href="<?php echo base_url(); ?>owner/lihatdelivery">
						<span class="menu-text"> Lihat Delivery </span>
					</a>
				</li>
			</ul>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>owner/search">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text"> Search </span>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>owner/reminder">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text"> Reject </span>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>owner/pemeriksaan">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text"> Report </span>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>owner/evaluasi">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text"> Evaluasi </span>
            </a>
        </li>
        <li>
            <a href="<?php echo base_url(); ?>owner/kritsar">
                <i class="menu-icon fa fa-ship"></i>
                <span class="menu-text"> Rekomendasi
                 </span>
            </a>
        </li>
        <!--Widgets-->
        
            
    </ul>
    <!-- /Sidebar Menu -->
</div>