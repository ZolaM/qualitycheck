<div class="page-sidebar bg-blue" id="sidebar">
<!-- <img src="<php echo base_url();>assets/img/attach-red.png"> -->
                <!-- Page Sidebar Header-->
                
                <!-- /Page Sidebar Header -->
                <!-- Sidebar Menu -->
                <ul class="nav sidebar-menu bg-blue">
                    <!--Dashboard-->
                    <li>
                        <a href="<?php echo base_url(); ?>admin/backhome">
                            <i class="menu-icon fa fa-anchor"></i>
                            <span class="menu-text"> Back To Home </span>
                        </a>
                    </li>
                    <!--Databoxes-->
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Kelola Project </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="<?php echo base_url(); ?>admin/suntingpro">
                                    <span class="menu-text">Sunting Project</span>
                                </a>
                            </li>

                            <!-- <li>
                                <a href="#">
                                    <span class="menu-text">Hapus Project</span>
                                </a>
                            </li>   -->                          
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Identifikasi Komponen</span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/itp">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/hullcon">
                                    <span class="menu-text">Hull Construction</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/machout">
                                    <span class="menu-text">Machinery Outfitting</span>
                                </a>
                            </li>                            
                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/hullout">
                                    <span class="menu-text">Hull Outfitting</span>
                                </a>
                            </li>   
                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/elecout">
                                    <span class="menu-text">Electrical Outfitting</span>
                                </a>
                            </li>  
                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/lihatitpiden">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li> 
                            <li>
                                <a href="<?php echo base_url(); ?>idenmat/lihatlist">
                                    <span class="menu-text">Lihat List</span>
                                </a>
                            </li>   
                        </ul>
                    </li>        
                    
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Fabrikasi </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="<?php echo base_url(); ?>fabrikasi/itpfab">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>fabrikasi/tambahfabrikasi">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>fabrikasi/lihatitpfab">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>fabrikasi/lihatfabrikasi">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  


                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Assembly </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="<?php echo base_url(); ?>assembly/itpass">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>assembly/tambahassembly">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>assembly/lihatitpass">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>assembly/lihatassembly">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Erection </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="<?php echo base_url(); ?>erection/itperec">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>erection/tambaherection">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>erection/lihatitperec">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>erection/lihaterection">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Outfitting </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
							<li>
                                <a href="<?php echo base_url(); ?>outfitting/ViewITPoutfitting">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>outfitting/tambahoutfitting">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>outfitting/LihatITPoutfitting">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>outfitting/lihatoutfitting">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Launching </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
							<li>
                                <a href="<?php echo base_url(); ?>launching/ViewITPlaunching">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>launching/tambahlaunching">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>launching/LihatITPlaunching">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>launching/lihatlaunching">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Commissioning </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
							<li>
                                <a href="<?php echo base_url(); ?>comm/ViewITPcomm">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>comm/tambahcomm">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>comm/LihatITPcomm">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>comm/lihatcomm">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Sea Trial </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
							<li>
                                <a href="<?php echo base_url(); ?>sea/ViewITPsea">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>sea/tambahsea">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>sea/LihatITPsea">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>sea/lihatsea">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li>  

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Delivery </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
							<li>
                                <a href="<?php echo base_url(); ?>delivery/viewITPdelivery">
                                    <span class="menu-text">Input ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>delivery/tambahdelivery">
                                    <span class="menu-text">Tambah Data</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>delivery/LihatITPdelivery">
                                    <span class="menu-text">Lihat ITP</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>delivery/lihatdelivery">
                                    <span class="menu-text">Lihat List Data</span>
                                </a>
                            </li>                            
                            
                        </ul>
                    </li> 
                    <li>
                        <a href="<?php echo base_url(); ?>home/search">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Search </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>home/reminder">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Reject </span>
                        </a>
                    </li>                     
                    <li>
                        <a href="<?php echo base_url(); ?>home/pemeriksaan">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Report </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>home/evaluasi">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Evaluasi </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>home/kritsar">
                            <i class="menu-icon fa fa-ship"></i>
                            <span class="menu-text"> Rekomendasi </span>
                        </a>
                    </li>
                    <!--Widgets-->                    
                    
                </ul>
                <!-- /Sidebar Menu -->
            </div>