<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

  /* default */
  public function index()	{
  }
  
  public function ambillistproject($kode=NULL){
    //$nilai = $this->input->post('nama');
    $this->load->model('Projectmodel');
    $query = $this->Projectmodel->ambillistproject();
    echo json_encode($query->result_array());
  }

  public function ambildetailproject($kode=NULL){
    $nilai = $this->input->post('id');
    $this->load->model('Projectmodel');
    $query = $this->Projectmodel->ambildetailproject($nilai);
    echo json_encode($query->result_array());
  }

  public function ambiljenisidenmat($kode=NULL){
    $nilai = $this->input->post('nilai');
    $this->load->model('Barangmodel');
    $query = $this->Barangmodel->ambiljenis($nilai);
    echo json_encode($query->result_array());
  }

  public function ambilitem($kode=NULL){
    $nilai = $this->input->post('nilai');
    $bagian = $this->input->post('bagian');
    $this->load->model('Barangmodel');
    $query = $this->Barangmodel->ambilitem($nilai, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambillistiden($kode=NULL){
    $idproj = $this->input->post('idproj');
    $nilai = $this->input->post('nilai');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambillistiden($idproj, $nilai);
    echo json_encode($query->result_array());
  }

  public function ambildetailiden($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambildetailiden($idproj, $idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilitemiden($kode=NULL){
    $idenmat = $this->input->post('id_iden');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambilitemiden($idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilulangiden($kode=NULL){
    $idproj = $this->input->post('idproj');
    $bagian = $this->input->post('bagian');
    $idenparsing = $this->input->post('id_iden');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambilulangiden($idproj, $bagian, $idenparsing);
    echo json_encode($query->result_array());
  }

  public function ambilkomponen($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);
    $bagian = $this->input->post('bagian');
    
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambilkompmo($bagian, $keyword);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->nama_komp;
    echo json_encode($json_array);
  }

  public function ambilprosesfabrikasi($kode=NULL){
    $bagfabrikasi = $this->input->post('nilai');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambilprosesfabrikasi($bagfabrikasi);
    echo json_encode($query->result_array());
  }

  public function ambilitemfabrikasi($kode=NULL){
    $bagian = $this->input->post('bagian');
    $proses = $this->input->post('proses');
    $this->load->model('Barangmodel');
    $query = $this->Barangmodel->ambilitemfabrikasi($bagian, $proses);
    echo json_encode($query->result_array());
  }

  public function ambilnamakomp($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);
    $bagian = $this->input->post('bagian');
    $idproj = $this->input->post('idproj');
    
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambilnamakomp($bagian, $keyword, $idproj);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->nama_komp;
    echo json_encode($json_array);
  }

  public function ceknamaiden($kode=NULL){
    $nama = $this->input->post('nama');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ceknamaiden($nama);
    echo json_encode($query->result_array());
  }

  public function ambillistfabrikasi($kode=NULL){
    $idproj = $this->input->post('idproj');
    $nilai = $this->input->post('nilai');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambillistfabrikasi($idproj, $nilai);
    echo json_encode($query->result_array());
  }

  public function ambildetailfab($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambildetailfab($idproj, $idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilitemfab($kode=NULL){
    $idenmat = $this->input->post('id_iden');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambilitemfab($idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilulangfab($kode=NULL){
    $idproj = $this->input->post('idproj');
    $bagian = $this->input->post('bagian');
    $idenparsing = $this->input->post('id_iden');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambilulangfab($idproj, $bagian, $idenparsing);
    echo json_encode($query->result_array());
  }


  //========================================= Assembly ====================================================

  public function ambilprosesassembly($kode=NULL){    
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambilprosesassembly();
    echo json_encode($query->result_array());
  }

  public function ambilitemassembly($kode=NULL){    
    $proses = $this->input->post('proses');
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambilitemassembly($proses);
    echo json_encode($query->result_array());
  }

  public function ambillistassembly($kode=NULL){ 
    $idproj = $this->input->post('idproj');       
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambillistassembly($idproj);
    echo json_encode($query->result_array());
  }

  public function ambildetailassembly($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambildetailassembly($idproj, $idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilulangitemass($kode=NULL){    
    $idassembly = $this->input->post('id_iden');
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambilulangitemass($idassembly);
    echo json_encode($query->result_array());
  }

  //================================= erection =======================================

  public function ambilproseserection($kode=NULL){    
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambilproseserection();
    echo json_encode($query->result_array());
  }

  public function ambilitemerection($kode=NULL){    
    $proses = $this->input->post('proses');
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambilitemerection($proses);
    echo json_encode($query->result_array());
  }

  public function ambilnamaassembly($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);    
    $idproj = $this->input->post('idproj'); 
    
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambilnamaassembly($keyword, $idproj);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->blok_assembly;
    echo json_encode($json_array);
  }

  public function ceknamaas($kode=NULL){    
    $nama = $this->input->post('nama');
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ceknamaas($nama);
    echo json_encode($query->result_array());
  }

  public function ambillisterection($kode=NULL){ 
    $idproj = $this->input->post('idproj');       
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambillisterection($idproj);
    echo json_encode($query->result_array());
  }

  public function ambildetailerection($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambildetailerection($idproj, $idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilulangitemer($kode=NULL){    
    $iderection = $this->input->post('id_iden');
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambilulangitemer($iderection);
    echo json_encode($query->result_array());
  }

  //=================================== outfitting ============================================

   public function ambiloutfitting($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);    
    $bagian = $this->input->post('bagian'); 
    $idproj = $this->input->post('idproj');
    
    $this->load->model('Outfittingmodel');
    $query = $this->Outfittingmodel->ambiloutfitting($keyword, $bagian, $idproj);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->nama_komp;
    echo json_encode($json_array);
  }


  public function ambilitemout($kode=NULL){    
    $itemselected = $this->input->post('itemselected');
    $bagian = $this->input->post('bagian');
    $this->load->model('Outfittingmodel');
    $query = $this->Outfittingmodel->ambilitemout($itemselected, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambillistoutfitting($kode=NULL){    
    $bagian = $this->input->post('nilai');
    $idproj = $this->input->post('idproj');
    $this->load->model('Outfittingmodel');
    $query = $this->Outfittingmodel->ambillistoutfitting($bagian, $idproj);
    echo json_encode($query->result_array());
  }

  public function ambildetailoutfitting($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $bagian = $this->input->post('bagian');
    $this->load->model('Outfittingmodel');
    $query = $this->Outfittingmodel->ambildetailoutfitting($idproj, $idenmat, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambilulangitemout($kode=NULL){    
    $idout = $this->input->post('id_iden');
    $this->load->model('Outfittingmodel');
    $query = $this->Outfittingmodel->ambilulangitemout($idout);
    echo json_encode($query->result_array());
  }


//========================= Launching ===================================================================
   public function ambillaunching($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);    
    $bagian = $this->input->post('bagian'); 
    
    $this->load->model('Launchingmodel');
    $query = $this->Launchingmodel->ambillaunching($keyword, $bagian);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->nama_komplau;
    echo json_encode($json_array);
  }

  public function ambilitemlau($kode=NULL){    
    $itemselected = $this->input->post('itemselected');
    $bagian = $this->input->post('bagian');
    $this->load->model('Launchingmodel');
    $query = $this->Launchingmodel->ambilitemlau($itemselected, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambillistlaunching($kode=NULL){    
    $bagian = $this->input->post('nilai');
    $idproj = $this->input->post('idproj');
    $this->load->model('Launchingmodel');
    $query = $this->Launchingmodel->ambillistlaunching($bagian, $idproj);
    echo json_encode($query->result_array());
  }


  public function ambildetaillaunching($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $bagian = $this->input->post('bagian');
    $this->load->model('Launchingmodel');
    $query = $this->Launchingmodel->ambildetaillaunching($idproj, $idenmat, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambilulangitemlau($kode=NULL){    
    $idlau = $this->input->post('id_iden');
    $this->load->model('Launchingmodel');
    $query = $this->Launchingmodel->ambilulangitemlau($idlau);
    echo json_encode($query->result_array());
  }


//============================ Commissioning =====================================================


  public function ambilcomm($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);    
    $bagian = $this->input->post('bagian'); 
    
    $this->load->model('Comodel');
    $query = $this->Comodel->ambilcomm($keyword, $bagian);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->pengujian_comm;
    echo json_encode($json_array);
  }


  public function ambilitemcom($kode=NULL){    
    $itemselected = $this->input->post('itemselected');
    $bagian = $this->input->post('bagian');
    $this->load->model('Comodel');
    $query = $this->Comodel->ambilitemcom($itemselected, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambillistcomm($kode=NULL){    
    $bagian = $this->input->post('nilai');
    $idproj = $this->input->post('idproj');
    $this->load->model('Comodel');
    $query = $this->Comodel->ambillistcomm($bagian, $idproj);
    echo json_encode($query->result_array());
  }


  public function ambildetailcomm($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    $bagian = $this->input->post('bagian');
    $this->load->model('Comodel');
    $query = $this->Comodel->ambildetailcomm($idproj, $idenmat, $bagian);
    echo json_encode($query->result_array());
  }


  public function ambilitemcomm($kode=NULL){    
    $idcomm = $this->input->post('id_iden');
    $this->load->model('Comodel');
    $query = $this->Comodel->ambilitemcomm($idcomm);
    echo json_encode($query->result_array());
  }



//================================= Sea trial ===========================================================

  public function ambilsea($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);    
    $bagian = $this->input->post('bagian'); 
    
    $this->load->model('Seamodel');
    $query = $this->Seamodel->ambilsea($keyword, $bagian);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->pengujian_sea;
    echo json_encode($json_array);
  }

  public function ambilitemsea($kode=NULL){    
    $itemselected = $this->input->post('itemselected');
    $bagian = $this->input->post('bagian');
    $this->load->model('Seamodel');
    $query = $this->Seamodel->ambilitemsea($itemselected, $bagian);
    echo json_encode($query->result_array());
  }

  public function ambillistsea($kode=NULL){    
    $bagian = $this->input->post('nilai');
    $idproj = $this->input->post('idproj');
    $this->load->model('Seamodel');
    $query = $this->Seamodel->ambillistsea($bagian, $idproj);
    echo json_encode($query->result_array());
  }

  public function ambildetailsea($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');
    //$bagian = $this->input->post('bagian');
    $this->load->model('Seamodel');
    $query = $this->Seamodel->ambildetailsea($idproj, $idenmat);
    echo json_encode($query->result_array());
  }

  public function ambilulangitemsea($kode=NULL){    
    $idcomm = $this->input->post('id_iden');
    $this->load->model('Seamodel');
    $query = $this->Seamodel->ambilulangitemsea($idcomm);
    echo json_encode($query->result_array());
  }

  //================================ Delivery ========================================

  public function ambildelivery($kode=NULL){
    //$keyword = $this->uri->segment(3);
    $keyword = $this->input->post('kode',TRUE);        
    
    $this->load->model('Deliverymodel');
    $query = $this->Deliverymodel->ambildelivery($keyword);
     $json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->nama_dokumen;
    echo json_encode($json_array);
  }

  public function ambillistdelivery($kode=NULL){        
    $idproj = $this->input->post('idproj');
    $this->load->model('Deliverymodel');
    $query = $this->Deliverymodel->ambillistdelivery($idproj);
    echo json_encode($query->result_array());
  }

  public function ambildetaildelivery($kode=NULL){
    $idproj = $this->input->post('idproj2');
    $idenmat = $this->input->post('idenmat');    
    $this->load->model('Deliverymodel');
    $query = $this->Deliverymodel->ambildetaildelivery($idproj, $idenmat);
    echo json_encode($query->result_array());
  }
  
  public function ambilulangitemdelivery($kode=NULL){    
    $iddeliv = $this->input->post('id_iden');
    $this->load->model('Deliverymodel');
    $query = $this->Deliverymodel->ambilulangitemdelivery($iddeliv);
    echo json_encode($query->result_array());
  }

  
  //=========================================== remainder =========================================

  public function remainder($kode=NULL){   
    $idproj = $this->input->post('idproj'); 
    $nilai = $this->input->post('nilai'); 
    $tgl = $this->input->post('wkt');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->remainder($idproj, $nilai, $tgl);
    echo json_encode($query->result_array());
  }

  //========================================== evaluasi ======================================
  public function dataeval($kode=NULL){   
    $idproj = $this->input->post('idproj'); 
    $nilai = $this->input->post('nilai'); 
    $tgl = $this->input->post('tgl'); 
    $tgl2 = $this->input->post('tgl2');
    $tglperiksa = $this->input->post('periksa'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->dataeval($idproj, $nilai, $tgl, $tgl2, $tglperiksa);
    echo json_encode($query->result_array());
  }

  //========================================== logic search ======================================

  public function logicsearch($kode=NULL){   
    $idproj = $this->input->post('idproj'); 
    $nilai = $this->input->post('nilai'); 
    $keyword = $this->input->post('keyword');
    $tabelkomp = $this->input->post('tabelkomp');
    $awal = $this->input->post('awal');
    $akhir = $this->input->post('akhir');
    $tglperiksa = $this->input->post('tperiksa');

    $this->load->model('Datamodel');
    $query = $this->Datamodel->logicsearch($idproj, $nilai, $keyword, $tabelkomp, $awal, $akhir, $tglperiksa);
    //print_r($query);
    echo json_encode($query->result_array());
  }

  //========================================= Registrasi dan Pendaftaran ========================

  public function ambillistpendaftar($kode=NULL){       
    $nilai = $this->input->post('nilai'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->ambillistpendaftar($nilai);
    echo json_encode($query->result_array());
  }

  public function ambildetailpendaftar($kode=NULL){       
    $id_pendaftar = $this->input->post('idenmat'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->ambildetailpendaftar($id_pendaftar);
    echo json_encode($query->result_array());
  }

  public function setujudaftar($kode=NULL){       
    $id = $this->input->post('id'); 
    $user = $this->input->post('user'); 
    $password = $this->input->post('password');
    $nama = $this->input->post('nama');
    $tipepengguna = $this->input->post('tipepengguna');
    $project = $this->input->post('project');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->setujudaftar($id, $user, $password, $nama, $tipepengguna, $project);
    
  }

  public function hapuspendaftar($kode=NULL){       
    $id = $this->input->post('idpendaftar'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->hapuspendaftar($id);
    //print_r($query);
    echo json_encode(array('res' => $query));
  }

// ===================================== Hapus Project ==================================================
  public function hapusproject($kode=NULL){       
    $id = $this->input->post('idpro'); 
    $this->load->model('Projectmodel');
    $query = $this->Projectmodel->hapusproject($id);
    echo json_encode(array('res' => $query));
  }
// ===================================== Hapus idenmat ==================================================
  public function hapusidenmat($kode=NULL){       
    $id = $this->input->post('ididenmat'); 
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->hapusidenmat($id);
    echo json_encode(array('res' => $query));
  }

// ===================================== Hapus fabrikasi ==================================================
  public function hapusfab(){
    $id = $this->input->post('idfab'); 
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->hapusfab($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus assembly ==================================================
  public function hapusass(){
    $id = $this->input->post('idass'); 
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->hapusass($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus erection ==================================================
  public function hapuserec(){
    $id = $this->input->post('iderec'); 
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->hapuserec($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus outfitting ==================================================
  public function hapusoutf(){
    $id = $this->input->post('idoutf'); 
    $this->load->model('Outfittingmodel');
    $query = $this->Outfittingmodel->hapusoutf($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus launching ==================================================
  public function hapuslaunch(){
    $id = $this->input->post('idlaunch'); 
    $this->load->model('Launchingmodel');
    $query = $this->Launchingmodel->hapuslaunch($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus commisioning ==================================================
  public function hapuscomm(){
    $id = $this->input->post('idcomm'); 
    $this->load->model('Comodel');
    $query = $this->Comodel->hapuscomm($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus seatrial ==================================================
  public function hapussea(){
    $id = $this->input->post('idsea'); 
    $this->load->model('Seamodel');
    $query = $this->Seamodel->hapussea($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Hapus delivery ==================================================
  public function hapusdel(){
    $id = $this->input->post('iddel'); 
    $this->load->model('Deliverymodel');
    $query = $this->Deliverymodel->hapusdel($id);
    echo json_encode(array('res' => $query));
  }

  // ===================================== Semua komponen ==================================================
  public function semuakomponen($kode = NULL){
    $kwd = $this->input->post('kode', TRUE); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->semuakomp($kwd);
    $json_array = array();
      foreach ($query as $hsl) {
        $json_array[] = $hsl->kompor;
      }
    echo json_encode($json_array);
  }

  public function pemeriksaan($kode=NULL){   
    $idproj = $this->input->post('idproj'); 
    $nilai = $this->input->post('nilai'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->pemeriksaan($idproj, $nilai);
    echo json_encode($query->result_array());
  }

  public function ambilkritsar($kode=NULL){   
    $idproj = $this->input->post('idproj'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->kritsar($idproj);
    echo json_encode($query->result_array());
  }

  
  public function ambildataks($kode=NULL){   
    $idks = $this->input->post('idkritsar'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->ambildataks($idks);
    echo json_encode($query->result_array());
  }

  public function updatekritsar($kode=NULL){   
    $idks = $this->input->post('idkritsar'); 
    $krit = $this->input->post('krit'); 
    $sar = $this->input->post('sar'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->updatekritsar($idks, $krit, $sar);
    echo json_encode(array('res' => $query));
  }  

  public function hapuskritsar($kode=NULL){   
    $idks = $this->input->post('idks'); 
    $this->load->model('Datamodel');
    $query = $this->Datamodel->hapuskritsar($idks);
    echo json_encode(array('res' => $query));
  }

  public function fabpro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->fabpro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function asspro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->asspro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function erpro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->erpro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function idenpro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->idenpro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function outpro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->outpro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function laupro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->laupro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function compro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->compro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function seapro(){
    $tgl = $this->input->post('tgl');
    $tgl2 = $this->input->post('tgl2');
    $this->load->model('Datamodel');
    $query = $this->Datamodel->seapro($tgl, $tgl2);
    echo json_encode($query->result_array());
  }

  public function ambilcliden($kode=NULL){
    $bagian = $this->input->post('bagian');
    $idproj = $this->input->post('idproj');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambilcliden($bagian, $idproj);
    echo json_encode($query->result_array());
  }

  public function ambilitemcliden(){
    $idkomp = $this->input->post('idkomp');
    $idproj = $this->input->post('idproj');
    $this->load->model('Idenmodel');
    $query = $this->Idenmodel->ambilitemcliden($idkomp, $idproj);
    echo json_encode($query->result_array());
  }

  public function ambilclass(){
    $idproj = $this->input->post('idproj');
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambilclass($idproj);
    echo json_encode($query->result_array());
  }

  public function ambilitemclass(){
    $idproj = $this->input->post('idproj');
    $proses = $this->input->post('proses');
    $this->load->model('Assemblymodel');
    $query = $this->Assemblymodel->ambilitemclass($idproj, $proses);
    echo json_encode($query->result_array());
  }

  public function ambilclfab(){
    $idproj = $this->input->post('idproj');
    $bagian = $this->input->post('bagian');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambilclfab($idproj, $bagian);
    echo json_encode($query->result_array());
  }

   public function ambilitemclfab(){
    $idproj = $this->input->post('idproj');
    $proses = $this->input->post('proses');
    $bagian = $this->input->post('bagian');
    $this->load->model('Fabrikasimodel');
    $query = $this->Fabrikasimodel->ambilitemclfab($idproj, $bagian, $proses);
    echo json_encode($query->result_array());
  }

  public function ambilclerec(){
    $idproj = $this->input->post('idproj');
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambilclerec($idproj);
    echo json_encode($query->result_array());
  }

  public function ambilitemclerec(){
    $idproj = $this->input->post('idproj');
    $proses = $this->input->post('proses');
    $this->load->model('Erectionmodel');
    $query = $this->Erectionmodel->ambilitemclerec($idproj, $proses);
    echo json_encode($query->result_array());
  }

  public function ambilitemcldelivery(){
	  $nama = $this->input->post('nama');
	  $idproj = $this->input->post('idproj');
	  $this->load->model('deliverymodel');
	  $query = $this->deliverymodel->ambilItemClDelivery($nama,$idproj);
	  echo json_encode($query->result_array());  
  }
  
  public function ambilclsea(){
    $bagian = $this->input->post('bagian');
	$idproj = $this->input->post('idproj');
    $this->load->model('seamodel');
    $query = $this->seamodel->ambilitemclsea($bagian,$idproj);
    echo json_encode($query->result_array());
  }
  
   public function ambilitemclsea(){
    $id = $this->input->post('id');
    $this->load->model('seamodel');
    $query = $this->seamodel->ambilitemclseaByKomponen($id);
    echo json_encode($query->result_array());
   }
  
  public function ambilclcomm(){
    $bagian = $this->input->post('bagian');
	$idproj = $this->input->post('idproj');
    $this->load->model('comodel');
    $query = $this->comodel->ambilitemclcomm($bagian,$idproj);
    echo json_encode($query->result_array());
  }
  
   public function ambilitemclcomm(){
    $id = $this->input->post('id');
    $this->load->model('comodel');
    $query = $this->comodel->ambilitemclcommByKomponen($id);
    echo json_encode($query->result_array());
  }
  
  public function ambilcllaunching(){
    $bagian = $this->input->post('bagian');
	$idproj = $this->input->post('idproj');
    $this->load->model('launchingmodel');
    $query = $this->launchingmodel->ambilitemcllaunching($bagian,$idproj);
    echo json_encode($query->result_array());
  }
  
   public function ambilitemcllaunching(){
    $id = $this->input->post('id');
    $this->load->model('launchingmodel');
    $query = $this->launchingmodel->ambilitemcllaunchingByKomponen($id);
    echo json_encode($query->result_array());
  }
  
  public function ambilcloutfitting(){
    $bagian = $this->input->post('bagian');
	 $idproj = $this->input->post('idproj');
    $this->load->model('outfittingmodel');
    $query = $this->outfittingmodel->ambilitemcloutfitting($bagian,$idproj);
    echo json_encode($query->result_array());
  }
  
   public function ambilitemcloutfitting(){
    $id = $this->input->post('id');
    $this->load->model('outfittingmodel');
    $query = $this->outfittingmodel->ambilitemcloutfittingByKomponen($id);
    echo json_encode($query->result_array());
  }

}



/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */


