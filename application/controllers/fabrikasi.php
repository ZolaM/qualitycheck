<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Home controller class
 * This is only viewable to those members that are logged in
 */
 class Fabrikasi extends CI_Controller{
    function __construct(){
        parent::__construct();
    }
    
    public function index(){
        if($this->session->userdata('hak_akses') == null) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('home');
        }
    }

    //====================================== Link =================================================================
    
    public function tambahfabrikasi($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            $this->load->model('Fabrikasimodel');
            $data['id'] = $this->Fabrikasimodel->ambilmaxid()+1;
            $data['hasil'] = $hasil;
            $this->load->view('administrator/fabrikasi/tambahfabrikasi', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatfabrikasi($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            /*$this->load->model('Idenmodel');
            $data['id'] = $this->Idenmodel->ambilmaxid()+1;*/
            $data['hasil'] = $hasil;
            $this->load->view('administrator/fabrikasi/lihatfabrikasi', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function itpfab($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            $data['hasil'] = $hasil;
            $this->load->view('administrator/fabrikasi/clfab', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatitpfab($hasil = null){
        if($this->session->userdata('hak_akses') != null) {
            $data['hasil'] = $hasil;
            $this->load->view('administrator/fabrikasi/listclfab', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function suntingitpfab($proses, $bagian){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            $proses = str_replace('%20', ' ', $proses);
            $bagian = str_replace('%20', ' ', $bagian);
            $data['proses'] = $proses;
            $data['bagian'] = $bagian;
            $data['hasil'] = null;
            $this->load->view('administrator/fabrikasi/suntingclfab', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
    }

    public function submititpfab(){
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->idproj = $barang->idproj;
            $obj->bag = $barang->bagian;
            $obj->proses = $barang->proses;
            $obj->nama_item = $barang->nama_item;
            $obj->galangan = $barang->galangan;
            $obj->owner = $barang->owner;
            $obj->cls = $barang->cls;
            $obj->sr = $barang->sr;
            $obj->std = $barang->std;

            array_push($itemlist, $obj);
        }

        $this->load->model('Fabrikasimodel');
        if($this->Fabrikasimodel->submititpfab($itemlist)){
            $data['hasil'] = "sukses";
        }
        else{
            $data['hasil'] = "gagal";
        }
        $this->load->view('administrator/fabrikasi/clfab', $data);
    }

    public function edititpfab(){
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->idproj = $barang->idproj;
            $obj->bag = $barang->bagian;
            $obj->proses = $barang->proses;
            $obj->nama_item = $barang->nama_item;
            $obj->galangan = $barang->galangan;
            $obj->owner = $barang->owner;
            $obj->cls = $barang->cls;
            $obj->sr = $barang->sr;
            $obj->std = $barang->std;
            $obj->status = $barang->status;

            array_push($itemlist, $obj);
        }

        $this->load->model('Fabrikasimodel');
        if($this->Fabrikasimodel->edititpfab($itemlist)){
            $data['hasil'] = "sukses";
        }
        else{
            $data['hasil'] = "gagal";
        }
        $this->load->view('administrator/fabrikasi/listclfab', $data);
    }

    public function suntingfabrikasi($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {  
            $idenparsing = $this->input->post('idenparsing');
            $idproj = $this->input->post('idproj');
            $bagian = $this->input->post('bagian');
            $data['idenparsing'] = $idenparsing;
            $data['idproj'] = $idproj;
            $data['bagian'] = $bagian;
            $data['hasil'] = $hasil;
            $this->load->view('administrator/fabrikasi/suntingfabrikasi', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function suntingfabrikasii($idpro, $idfab, $bag, $hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {  
            $idenparsing = $idfab;
            $idproj = $idpro;
            $bagian = str_replace("%20", " ", $bag);
            $data['idenparsing'] = $idenparsing;
            $data['idproj'] = $idproj;
            $data['bagian'] = $bagian;
            $data['hasil'] = $hasil;
            $this->load->view('administrator/fabrikasi/suntingfabrikasi', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    //======================================================================================================================


    //================================= Submit Identifikasi Material =======================================================


     public function submitfabrikasi($hasil = null){

        $idproj = $this->input->post('idproj');
        $idmat = $this->input->post('id_fabrikasi');
        $jenis = $this->input->post('jenis');
        $bagian = $this->input->post('bagian');
        $proses = $this->input->post('proses');
        $namakomp = $this->input->post('namakomp');
        $idkomp = $this->input->post('idkomp');
        $namablok = $this->input->post('namablok');
        $pros = $this->input->post('pros');
        $alat = $this->input->post('alat');
        $rules = $this->input->post('rules');

        $qcinspec = $this->input->post('qcinspec');
        $qacoor = $this->input->post('qacoor');
        $classsur = $this->input->post('classsur');
        $ownersur = $this->input->post('ownersur');
        $statereg = $this->input->post('statereg');
        $tanggalperiksa = $this->input->post('tanggalperiksa');
        $status = $this->input->post('status');

        date_default_timezone_set("Asia/Jakarta");
        $wktinp = date('Y-m-d H:i:s');
        $oleh = $this->session->userdata('nama_pengguna');
        
        if($status == "OK"){            
            $reinspek = "2001-01-01";
            $rekomendasi = "null";
        }
        else{
            $reinspek = $this->input->post('reinspek');
            $rekomendasi = $this->input->post('rekomendasi');    
        }
        
        $path = $path2 = $path3 = $path4 = null;

        if($_FILES['gambar1']['name'])
        {
            $filename = $_FILES['gambar1']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path = 'upload/fabrikasi/'.$filename;
        }

        if($_FILES['gambar2']['name']){
            $filename = $_FILES['gambar2']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path2 = 'upload/fabrikasi/'.$filename;
        }

        if($_FILES['gambar3']['name']){
            $filename = $_FILES['gambar3']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path3 = 'upload/fabrikasi/'.$filename;
        }

        if($_FILES['gambar4']['name']){
            $filename = $_FILES['gambar4']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path4 = 'upload/fabrikasi/'.$filename;
        }
        
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->nama = $barang->nama;
            $obj->item = $barang->item;
            $obj->standard = $barang->standard;
            $obj->pihak = $barang->pihak;

            array_push($itemlist, $obj);

        }


        $this->load->model('Fabrikasimodel');

        if($this->Fabrikasimodel->tambahfabrikasi($idproj, $idmat, $proses, $jenis, $namakomp, $idkomp, $namablok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $bagian, $wktinp, $oleh)){
            if ($this->Fabrikasimodel->tambahitemfabrikasi($idmat, $itemlist)) {
                $ctr = 0;
                    $scan = array();
                    $config['upload_path'] = './upload/fabrikasi';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2048000';

                    $this->load->library('upload', $config);

                    for ($i = 1; $i <=4; $i++){
                        if (!empty($_FILES['gambar'.$i]['name'])) {
                            if (!$this->upload->do_upload('gambar'.$i))
                            {
                                echo $this->upload->display_errors();
                                $scan[$i] = array('file_name' => NULL);
                                $ctr++;
                            }
                            else
                            {
                                $scan[$i] = $this->upload->data();
                            }
                        }
                        else{
                            $scan[$i] = array('file_name' => NULL);
                        }

                    }

                if ( $ctr > 0)
                {
                    $error = array('error' => $this->upload->display_errors());
                    echo $this->upload->display_errors();
                    /*$hasil = 'gagal';
                     redirect('idenmat/hullcon/'. $hasil);*/
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                    /*$hasil = 'sukses';
                    redirect('fabrikasi/tambahfabrikasi/'. $hasil);*/

                    ob_start();
                    require 'fpdf.php';

                    $pdf = new FPDF('P', 'cm', 'A4');
                    $pdf->SetAutoPageBreak(true);
                    $pdf->AddFont('courier', '', 'courier.php');
                    $pdf->AddFont('timesb', '', 'timesb.php');
                    $pdf->AddFont('times', '', 'times.php');
                    $pdf->SetMargins(1.5, 1.5, 1.5, 1.5);
                    $pdf->AddPage('P', 'A4');
                    $pdf->SetFont('timesb', '', 22);

                    //===================== Judul dan PT ==========================================
                    $pdf->Image('./assets/img/PIN.png', 1.5, 1.5,4,2.4);                     

                    $pdf->Cell(22, 0, 'PT. Putra Rahwana Shipyard', 0, 1, "C");
                    $pdf->SetFont('times', '', 16);
                    $pdf->Ln(0.5);
                    $pdf->Cell(22, 1, 'Jalan Gatot Subroto No. 32, Ketapang', 0, 1, "C");
                    $pdf->Ln(0.4);
                    $pdf->Cell(22, 0, 'Banyuwangi, Jawa Timur ', 0, 1, "C");
                    $pdf->Ln(0.1);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.7,24);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.8,24);
                    $pdf->Ln(1.8);
                    $pdf->SetFont('timesb', '', 16);

                    $pdf->Cell(26, 0.4, 'Fabrikasi '.$bagian, 0, 1, "L");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(0.7);

                    //=========================== Isi Tahapan ===================================
                    if ($bagian == "Hull Construction") { 
                        $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
                        $pdf->Ln(0.4);                       
                        $pdf->Cell(4.2, 0.4, 'Nama Komponen', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namakomp, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'ID Komponen', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $idkomp, 0, 1, 'L');
                        $pdf->Ln(0.4); 
                        $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
                        $pdf->Ln(0.4); 
                    }
                    else{
                        $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'Nama Outfitting', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namakomp, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'ID Outfitting', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $idkomp, 0, 1, 'L');
                        $pdf->Ln(0.4); 
                        $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
                        $pdf->Ln(0.4); 
                    }
                    
                    

                    //=========================== Info Umum ====================================
                    $pdf->Cell(4.2, 0.4, 'Tanggal Periksa', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4,  $tanggalperiksa, 0, 1, 'L');
                    $pdf->Ln(0.4);
                    $pdf->Cell(4.2, 0.4, 'Status', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $status, 0, 1, 'L');
                    $pdf->Ln(1);

                    

                    //============================ item ===============================
                    
                    $pdf->SetFont('timesb', '', 12);
                    $header = array('Nama Item', 'Isi Item', 'Standard Item', 'Pihak Pemeriksa');
                    $w = array(7.5, 3.4, 3.4, 3.4);
                    // Header tabel
                    for($i=0;$i<count($header);$i++)
                        $pdf->Cell($w[$i],1,$header[$i],1,0,'C');
                    $pdf->Ln();

                    //isi tabel
                    $pdf->SetFont('times', '', 11);
                    foreach($daftarBarang as $row)
                    {
                        $pdf->Cell($w[0],1,$row->nama,'LR',0,'L');
                        $pdf->Cell($w[1],1,$row->item,'LR',0,'L');
                        $pdf->Cell($w[2],1,$row->standard,'LR',0,'L');            
                        $pdf->Cell($w[3],1,$row->pihak,'LR',0,'L');            
                        $pdf->Ln();
                    }
                    // Closing line
                    $pdf->Cell(array_sum($w),0,'','T');

                    //====================== kalo  Reject ini tempatnya =====================
                    $pdf->Ln(1);  
                    $pdf->SetFont('times', '', 13);
                    if($status == "Reject"){
                        $pdf->Cell(4.2, 0.4, 'Tanggal Reinspeksi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $reinspek, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'Rekomendasi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $rekomendasi, 0, 1, 'L');
                        $pdf->Ln(1.2);
                    }
                    
                    $pdf->SetFont('times', '', 13);

                    //============================== TTD ====================================

                    $pdf->Cell(18, 0.4, 'Yang Memeriksa,', 0, 0, "C");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'QC Inspector', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qcinspec, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'QA Coordinator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qacoor, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Class Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $classsur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Owner Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $ownersur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'State Regulator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $statereg, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'Diinput Oleh', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $oleh, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    date_default_timezone_set("Asia/Jakarta");
                    $pdf->Cell(4.2, 0.4, 'Waktu Input', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $wktinp, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    // $pdf->Cell(5, 0, 'QC Inspector', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'QA Coordinator', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $qcinspec, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $qacoor , 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(5, 0, 'Class Surveyor', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'Owner Surveyor', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $classsur, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $ownersur, 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(18, 0.4, 'State Regulator,', 0, 0, "C");

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(18, 0.4, $statereg, 0, 0, "C");
                    
                    if(!empty($path)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 1', 0, 1, "L");
                        $pdf->Image($path, 1.5, 2, 18, 26);
                    }
                    
                    if(!empty($path2)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 2', 0, 1, "L");
                        $pdf->Image($path2, 1.5, 2, 18, 26);
                    }

                    if(!empty($path3)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 3', 0, 1, "L");
                        $pdf->Image($path3, 1.5, 2, 18, 26);
                    }

                    if(!empty($path4)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 4', 0, 1, "L");
                        $pdf->Image($path4, 1.5, 2, 18, 26);
                    }

                    /*$pdf->AddPage('P', 'A4');
                    $pdf->Image('./'.$path, 2, 2,15,25); */

                    $pdf->Output();
                    ob_end_flush();

                    
                }


            }
            else{
                echo "gagal";
            }
            
        }
        else{
                echo "gagal";
        }   
    }

    //=============================================================================================================================


    //===================================== Update Identifikasi Material ==========================================================

    public function updatefabrikasi($hasil = null){

        $idproj = $this->input->post('idproj');
        $idmat = $this->input->post('id_fabrikasi');
        $bagian = $this->input->post('bagian');
        $proses = $this->input->post('proses');
        $namakomp = $this->input->post('namakomp');
        $idkomp = $this->input->post('idkomp');
        $namablok = $this->input->post('namablok');
        $pros = $this->input->post('pros');
        $alat = $this->input->post('alat');
        $rules = $this->input->post('rules');

        $qcinspec = $this->input->post('qcinspec');
        $qacoor = $this->input->post('qacoor');
        $classsur = $this->input->post('classsur');
        $ownersur = $this->input->post('ownersur');
        $statereg = $this->input->post('statereg');
        $tanggalperiksa = $this->input->post('tanggalperiksa');
        $status = $this->input->post('status');

        date_default_timezone_set("Asia/Jakarta");
        $wktinp = date('Y-m-d H:i:s');
        $oleh = $this->session->userdata('nama_pengguna');
        
        if($status == "OK"){            
            $reinspek = "2001-01-01";
            $rekomendasi = "null";
        }
        else{
            $reinspek = $this->input->post('reinspek');
            $rekomendasi = $this->input->post('rekomendasi');    
        }

        $path = $path2 = $path3 = $path4 = null;

        if($_FILES['gambar1']['name'])
        {
            $filename = $_FILES['gambar1']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path = 'upload/fabrikasi/'.$filename;
        }
        else{
            $path = $this->input->post('imgdulu1');
        }

        if($_FILES['gambar2']['name']){
            $filename = $_FILES['gambar2']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path2 = 'upload/fabrikasi/'.$filename;
        }
        else{
            $path2 = $this->input->post('imgdulu2');
        }

        if($_FILES['gambar3']['name']){
            $filename = $_FILES['gambar3']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path3 = 'upload/fabrikasi/'.$filename;
        }
        else{
            $path3 = $this->input->post('imgdulu3');
        }

        if($_FILES['gambar4']['name']){
            $filename = $_FILES['gambar4']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path4 = 'upload/fabrikasi/'.$filename;
        }
        else{
            $path4 = $this->input->post('imgdulu4');
        }
        
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->nama = $barang->nama;
            $obj->item = $barang->item;
            $obj->standard = $barang->standard;
            $obj->pihak = $barang->pihak;

            array_push($itemlist, $obj);

        }


        $this->load->model('Fabrikasimodel');

        if($this->Fabrikasimodel->suntingfabrikasi($idproj, $idmat, $namakomp, $idkomp, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh)){
            if ($this->Fabrikasimodel->updateitemfabrikasi($idmat, $itemlist)) {
                $ctr = 0;
                    $scan = array();
                    $config['upload_path'] = './upload/fabrikasi';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2048000';

                    $this->load->library('upload', $config);

                    for ($i = 1; $i <=4; $i++){
                        if (!empty($_FILES['gambar'.$i]['name'])) {
                            if (!$this->upload->do_upload('gambar'.$i))
                            {
                                echo $this->upload->display_errors();
                                $scan[$i] = array('file_name' => NULL);
                                $ctr++;
                            }
                            else
                            {
                                $scan[$i] = $this->upload->data();
                            }
                        }
                        else{
                            $scan[$i] = array('file_name' => NULL);
                        }

                    }

                if ( $ctr > 0)
                {
                    $error = array('error' => $this->upload->display_errors());
                    echo $this->upload->display_errors();
                    
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                    /*$hasil = 'sukses';
                    redirect('fabrikasi/lihatfabrikasi/'. $hasil);*/

                    ob_start();
                    require 'fpdf.php';

                    $pdf = new FPDF('P', 'cm', 'A4');
                    $pdf->SetAutoPageBreak(true);
                    $pdf->AddFont('courier', '', 'courier.php');
                    $pdf->AddFont('timesb', '', 'timesb.php');
                    $pdf->AddFont('times', '', 'times.php');
                    $pdf->SetMargins(1.5, 1.5, 1.5, 1.5);
                    $pdf->AddPage('P', 'A4');
                    $pdf->SetFont('timesb', '', 22);

                    //===================== Judul dan PT ==========================================
                    $pdf->Image('./assets/img/PIN.png', 1.5, 1.5,4,2.4);                     

                    $pdf->Cell(22, 0, 'PT. Putra Rahwana Shipyard', 0, 1, "C");
                    $pdf->SetFont('times', '', 16);
                    $pdf->Ln(0.5);
                    $pdf->Cell(22, 1, 'Jalan Gatot Subroto No. 32, Ketapang', 0, 1, "C");
                    $pdf->Ln(0.4);
                    $pdf->Cell(22, 0, 'Banyuwangi, Jawa Timur ', 0, 1, "C");
                    $pdf->Ln(0.1);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.7,24);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.8,24);
                    $pdf->Ln(1.8);
                    $pdf->SetFont('timesb', '', 16);

                    $pdf->Cell(26, 0.4, 'Fabrikasi '.$bagian, 0, 1, "L");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(0.7);

                    //=========================== Isi Tahapan ===================================
                    if ($bagian == "Hull Construction") { 
                        $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
                        $pdf->Ln(0.4);                       
                        $pdf->Cell(4.2, 0.4, 'Nama Komponen', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namakomp, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'ID Komponen', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $idkomp, 0, 1, 'L');
                        $pdf->Ln(0.4); 
                        $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
                        $pdf->Ln(0.4);
                    }
                    else{
                        $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'Nama Outfitting', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namakomp, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'ID Outfitting', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $idkomp, 0, 1, 'L');
                        $pdf->Ln(0.4); 
                        $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
                        $pdf->Ln(0.4);
                    }
                    
                    

                    //=========================== Info Umum ====================================
                    $pdf->Cell(4.2, 0.4, 'Tanggal Periksa', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4,  $tanggalperiksa, 0, 1, 'L');
                    $pdf->Ln(0.4);
                    $pdf->Cell(4.2, 0.4, 'Status', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $status, 0, 1, 'L');
                    $pdf->Ln(1);

                    

                    //============================ item ===============================
                    
                    $pdf->SetFont('timesb', '', 12);
                    $header = array('Nama Item', 'Isi Item', 'Standard Item', 'Pihak Pemeriksa');
                    $w = array(7.5, 3.4, 3.4, 3.4);
                    // Header tabel
                    for($i=0;$i<count($header);$i++)
                        $pdf->Cell($w[$i],1,$header[$i],1,0,'C');
                    $pdf->Ln();

                    //isi tabel
                    $pdf->SetFont('times', '', 11);
                    foreach($daftarBarang as $row)
                    {
                        $pdf->Cell($w[0],1,$row->nama,'LR',0,'L');
                        $pdf->Cell($w[1],1,$row->item,'LR',0,'L');
                        $pdf->Cell($w[2],1,$row->standard,'LR',0,'L');            
                        $pdf->Cell($w[3],1,$row->pihak,'LR',0,'L');            
                        $pdf->Ln();
                    }
                    // Closing line
                    $pdf->Cell(array_sum($w),0,'','T');

                    //====================== kalo  Reject ini tempatnya =====================
                    $pdf->Ln(1);  
                    $pdf->SetFont('times', '', 13);
                    if($status == "Reject"){
                        $pdf->Cell(4.2, 0.4, 'Tanggal Reinspeksi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $reinspek, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'Rekomendasi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $rekomendasi, 0, 1, 'L');
                        $pdf->Ln(1.2);
                    }
                    
                    $pdf->SetFont('times', '', 13);

                    //============================== TTD ====================================

                    $pdf->Cell(18, 0.4, 'Yang Memeriksa,', 0, 0, "C");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'QC Inspector', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qcinspec, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'QA Coordinator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qacoor, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Class Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $classsur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Owner Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $ownersur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'State Regulator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $statereg, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'Diinput Oleh', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $oleh, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    date_default_timezone_set("Asia/Jakarta");
                    $pdf->Cell(4.2, 0.4, 'Waktu Input', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $wktinp, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    // $pdf->Cell(5, 0, 'QC Inspector', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'QA Coordinator', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $qcinspec, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $qacoor , 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(5, 0, 'Class Surveyor', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'Owner Surveyor', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $classsur, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $ownersur, 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(18, 0.4, 'State Regulator,', 0, 0, "C");

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(18, 0.4, $statereg, 0, 0, "C");
                    
                    if(!empty($path)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 1', 0, 1, "L");
                        $pdf->Image($path, 1.5, 2, 18, 26);
                    }
                    
                    if(!empty($path2)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 2', 0, 1, "L");
                        $pdf->Image($path2, 1.5, 2, 18, 26);
                    }

                    if(!empty($path3)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 3', 0, 1, "L");
                        $pdf->Image($path3, 1.5, 2, 18, 26);
                    }

                    if(!empty($path4)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 4', 0, 1, "L");
                        $pdf->Image($path4, 1.5, 2, 18, 26);
                    }

                    /*$pdf->AddPage('P', 'A4');
                    $pdf->Image('./'.$path, 2, 2,15,25); */

                    $pdf->Output();
                    ob_end_flush();

                    
                }


            }
            else{
                echo "gagal";
            }
            
        }
        else{
                echo "gagal";
        }


        
        
    }


    public function cetakfabrikasi(){

        
        $idproj = $this->input->post('idproj');
        $idenmat = $this->input->post('idenparsing');

        $this->load->model('Fabrikasimodel');
        $query = $this->Fabrikasimodel->ambildetailfab($idproj, $idenmat);
        
        $jsonstr = json_encode($query->result_array());  

        foreach ($query->result() as $row)
        {
            
            $idproj = $row->id_project; 
            $jenis = $row->jenis_fabrikasi;
            $bagian = $row->bagian_fabrikasi;
            $proses = $row->proses_fabrikasi;
            $namakomp = $row->nama_kompfab;
            $idkomp = $row->id_kompfab;
            $namablok = $row->nama_blok;

            $qcinspec = $row->qc_inspecfab;
            $qacoor = $row->qa_coorfab;
            $classsur = $row->class_surfab;
            $ownersur = $row->owner_surfab;
            $statereg = $row->state_regfab;
            $tanggalperiksa = $row->tgl_periksafab;
            $status = $row->status_fab;
            $reinspek = $row->tgl_reinspeksifab;
            $rekomendasi = $row->rekomendasi_fab;
            $path = $row->path_gambarfab;
            $path2 = $row->path_gambarfab2;
            $path3 = $row->path_gambarfab3;
            $path4 = $row->path_gambarfab4;

            $wktinp = $row->input_fab;
            $oleh = $row->oleh_fab;

        }

         $querys = $this->Fabrikasimodel->ambilitemfab($idenmat);         


         ob_start();
        require 'fpdf.php';

        $pdf = new FPDF('P', 'cm', 'A4');
        $pdf->SetAutoPageBreak(true);
        $pdf->AddFont('courier', '', 'courier.php');
        $pdf->AddFont('timesb', '', 'timesb.php');
        $pdf->AddFont('times', '', 'times.php');
        $pdf->SetMargins(1.5, 1.5, 1.5, 1.5);
        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('timesb', '', 22);

        //===================== Judul dan PT ==========================================
        $pdf->Image('./assets/img/PIN.png', 1.5, 1.5,4,2.4);                     

        $pdf->Cell(22, 0, 'PT. Putra Rahwana Shipyard', 0, 1, "C");
        $pdf->SetFont('times', '', 16);
        $pdf->Ln(0.5);
        $pdf->Cell(22, 1, 'Jalan Gatot Subroto No. 32, Ketapang', 0, 1, "C");
        $pdf->Ln(0.4);
        $pdf->Cell(22, 0, 'Banyuwangi, Jawa Timur ', 0, 1, "C");
        $pdf->Ln(0.1);
        $pdf->Image('./assets/img/pembatasn.png', 0, 4.7,24);
        $pdf->Image('./assets/img/pembatasn.png', 0, 4.8,24);
        $pdf->Ln(1.8);
        $pdf->SetFont('timesb', '', 16);

        $pdf->Cell(26, 0.4, 'Fabrikasi '.$bagian, 0, 1, "L");
        $pdf->SetFont('times', '', 13);
        $pdf->Ln(0.7);

        //=========================== Isi Tahapan ===================================
        if ($bagian == "Hull Construction") { 
            $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
            $pdf->Ln(0.4);                       
            $pdf->Cell(4.2, 0.4, 'Nama Komponen', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $namakomp, 0, 1, 'L');
            $pdf->Ln(0.4);
            $pdf->Cell(4.2, 0.4, 'ID Komponen', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $idkomp, 0, 1, 'L');
            $pdf->Ln(0.4); 
            $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
            $pdf->Ln(0.4); 
        }
        else{
            $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
            $pdf->Ln(0.4);
            $pdf->Cell(4.2, 0.4, 'Nama Outfitting', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $namakomp, 0, 1, 'L');
            $pdf->Ln(0.4);
            $pdf->Cell(4.2, 0.4, 'ID Outfitting', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $idkomp, 0, 1, 'L');
            $pdf->Ln(0.4); 
            $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
            $pdf->Ln(0.4); 
        }
        
        

        //=========================== Info Umum ====================================
        $pdf->Cell(4.2, 0.4, 'Tanggal Periksa', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4,  $tanggalperiksa, 0, 1, 'L');
        $pdf->Ln(0.4);
        $pdf->Cell(4.2, 0.4, 'Status', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $status, 0, 1, 'L');
        $pdf->Ln(1);

        

        //============================ item ===============================
        
        $pdf->SetFont('timesb', '', 12);
        $header = array('Nama Item', 'Isi Item', 'Standard Item', 'Pihak Pemeriksa');
        $w = array(7.5, 3.4, 3.4, 3.4);
        // Header tabel
        for($i=0;$i<count($header);$i++)
            $pdf->Cell($w[$i],1,$header[$i],1,0,'C');
        $pdf->Ln();

        //isi tabel
        $pdf->SetFont('times', '', 11);
        foreach ($querys->result() as $rows)
        {
            $pdf->Cell($w[0],1,$rows->nama_itemfab,'LR',0,'L');
            $pdf->Cell($w[1],1,$rows->isi_itemfab,'LR',0,'L');
            $pdf->Cell($w[2],1,$rows->standard_itemfab,'LR',0,'L');            
            $pdf->Cell($w[3],1,$rows->pihak_itemfab,'LR',0,'L');            
            $pdf->Ln();
        }
        // Closing line
        $pdf->Cell(array_sum($w),0,'','T');

        //====================== kalo  Reject ini tempatnya =====================
        $pdf->Ln(1);  
        $pdf->SetFont('times', '', 13);
        if($status == "Reject"){
            $pdf->Cell(4.2, 0.4, 'Tanggal Reinspeksi', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $reinspek, 0, 1, 'L');
            $pdf->Ln(0.4);
            $pdf->Cell(4.2, 0.4, 'Rekomendasi', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $rekomendasi, 0, 1, 'L');
            $pdf->Ln(1.2);
        }
        
        $pdf->SetFont('times', '', 13);

        //============================== TTD ====================================

        $pdf->Cell(18, 0.4, 'Yang Memeriksa,', 0, 0, "C");
        $pdf->SetFont('times', '', 13);
        $pdf->Ln(1.0);

        $pdf->Cell(4.2, 0.4, 'QC Inspector', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $qcinspec, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'QA Coordinator', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $qacoor, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'Class Surveyor', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $classsur, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'Owner Surveyor', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $ownersur, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'State Regulator', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $statereg, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'Diinput Oleh', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $oleh, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    date_default_timezone_set("Asia/Jakarta");
                    $pdf->Cell(4.2, 0.4, 'Waktu Input', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $wktinp, 0, 1, 'L');
                    $pdf->Ln(0.4);


        // $pdf->Cell(5, 0, 'QC Inspector', 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, 'QA Coordinator', 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('times', '', 13);
        // $pdf->Cell(5, 0, $qcinspec, 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, $qacoor , 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('timesb', '', 13);
        // $pdf->Cell(5, 0, 'Class Surveyor', 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, 'Owner Surveyor', 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('times', '', 13);
        // $pdf->Cell(5, 0, $classsur, 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, $ownersur, 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('timesb', '', 13);
        // $pdf->Cell(18, 0.4, 'State Regulator,', 0, 0, "C");

        // $pdf->Ln(2.5);

        // $pdf->SetFont('times', '', 13);
        // $pdf->Cell(18, 0.4, $statereg, 0, 0, "C");

        if(!empty($path)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 1', 0, 1, "L");
            $pdf->Image($path, 1.5, 2, 18, 26);
        }
        
        if(!empty($path2)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 2', 0, 1, "L");
            $pdf->Image($path2, 1.5, 2, 18, 26);
        }

        if(!empty($path3)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 3', 0, 1, "L");
            $pdf->Image($path3, 1.5, 2, 18, 26);
        }

        if(!empty($path4)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 4', 0, 1, "L");
            $pdf->Image($path4, 1.5, 2, 18, 26);
        }

        /*$pdf->AddPage('P', 'A4');
        $pdf->Image('./'.$path, 2, 2,15,25); */

        $pdf->Output();
        ob_end_flush();

         


        

    }



 }