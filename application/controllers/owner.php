<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Home controller class
 * This is only viewable to those members that are logged in
 */
 class Owner extends CI_Controller{
    function __construct(){
        parent::__construct();
    }
    
    public function index(){
        if($this->session->userdata('hak_akses') == null) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('home');
        }
    }

    //====================================== Link =================================================================
    
    public function lihatpro($hasil = null){
        if($this->session->userdata('hak_akses') == 3) {
            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatpro', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }
   

    public function lihatiden($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatiden', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatfabrikasi($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatfabrikasi', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatassembly($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatassembly', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihaterection($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihaterection', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatoutfitting($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatoutfitting', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatlaunching($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatlaunching', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatcommissioning($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatcommissioning', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatseatrial($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatseatrial', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatdelivery($hasil = null){
        if($this->session->userdata('hak_akses') == 2 || $this->session->userdata('hak_akses') == 3 && $this->session->userdata('pilihan_project') != 0) {            
            $data['hasil'] = $hasil;
            $this->load->view('owner/lihatdelivery', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function reminder()
    {
        
        if($this->session->userdata('hak_akses') == null || $this->session->userdata('pilihan_project') == 0) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('owner/reminder');
        }

    }

    public function pemeriksaan()
    {
        
        if($this->session->userdata('hak_akses') == null || $this->session->userdata('pilihan_project') == 0) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('owner/pemeriksaan');
        }

    }

    public function search()
    {
        
        if($this->session->userdata('hak_akses') == null || $this->session->userdata('pilihan_project') == 0) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('owner/logicsearch');
        }

    }

    public function evaluasi()
    {
        
        if($this->session->userdata('hak_akses') == null || $this->session->userdata('pilihan_project') == 0) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('owner/evaluasi');
        }

    }

    public function kritsar()
    {
        
        if($this->session->userdata('hak_akses') == null || $this->session->userdata('pilihan_project') == 0) {
            redirect(base_url() . 'login');
        }
        else {
            $Data['hasil'] = '';
            $this->load->view('owner/kritsar', $Data);
        }

    }

    public function lihatitpiden($hasil = null){
        if($this->session->userdata('hak_akses') != null) {
            
            $data['hasil'] = $hasil;
            $this->load->view('owner/listcliden', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatitpfab($hasil = null){
        if($this->session->userdata('hak_akses') != null) {
            $data['hasil'] = $hasil;
            $this->load->view('owner/listclfab', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihatitpass(){
        if($this->session->userdata('hak_akses') != null) {
            $data['hasil'] = null;
            $this->load->view('owner/listclass', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
    }

    public function lihatitperec($hasil = null){
        if($this->session->userdata('hak_akses') != null) {
            $data['hasil'] = $hasil;
            $this->load->view('owner/listclerec', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }
	
	public function LihatITPoutfittingUser(){
		if($this->session->userdata('hak_akses') > 1 && $this->session->userdata('pilihan_project') != 0) {
				$this->load->model('deliverymodel');
				$idproj=$this->session->userdata('pilihan_project');
				
				$data['list']=$this->deliverymodel->ambilITP($idproj);
				$data['hasil']=NULL;
				$data['idproj']=$idproj;
				
				$this->load->view('owner/listcloutfitting',$data);
		}else{
            redirect(base_url() . 'login');    
        }
	}
	
	public function LihatITPlaunchingUser(){
		if($this->session->userdata('hak_akses') > 1 && $this->session->userdata('pilihan_project') != 0) {
				$idproj=$this->session->userdata('pilihan_project');
				$data['hasil']=NULL;
				
				$data['idproj']=$idproj;
				
				$this->load->view('owner/listcllaunching',$data);
		}else{
            redirect(base_url() . 'login');    
        }
	}
	
	public function LihatITPcommUser(){
		if($this->session->userdata('hak_akses') > 1 && $this->session->userdata('pilihan_project') != 0) {
				$this->load->model('comodel');
				$idproj=$this->session->userdata('pilihan_project');
				
				$data['list']=$this->comodel->ambilITP($idproj);
				$data['hasil']=NULL;
				$data['idproj']=$this->session->userdata('pilihan_project');
				
				$this->load->view('owner/listclcomm',$data);
		}else{
            redirect(base_url() . 'login');    
        }
	}
	
	public function LihatITPseaUser(){
		if($this->session->userdata('hak_akses') > 1 && $this->session->userdata('pilihan_project') != 0) {
			$idproj=$this->session->userdata('pilihan_project');
			
			$data['hasil']=NULL;
			$data['idproj']=$this->session->userdata('pilihan_project');
			
			$this->load->view('owner/listclsea',$data);
		}else{
            redirect(base_url() . 'login');    
        }
	}
	
	public function LihatITPdeliveryUser(){
		if($this->session->userdata('hak_akses') > 1 && $this->session->userdata('pilihan_project') != 0) {
				$this->load->model('deliverymodel');
				$idproj=$this->session->userdata('pilihan_project');
				
				$data['list']=$this->deliverymodel->ambilITP($idproj);
				
				$this->load->view('owner/listcldelivery',$data);
		}else{
            redirect(base_url() . 'login');    
        }
	}

 }