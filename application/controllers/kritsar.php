<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kritsar extends CI_Controller {

	public function saveKritSar(){
		$this->load->model('Datamodel');

		$kritik = $this->input->post('kritik');
		$saran = $this->input->post('saran');
		$Data['hasil'] = $this->Datamodel->saveKritSar($kritik, $saran);
		if ($this->session->userdata('hak_akses') == '1'){
			$this->load->view('administrator/kritsar', $Data);
		}
		else{
			$this->load->view('owner/kritsar', $Data);
		}
		
	}
}
?>