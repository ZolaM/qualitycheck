<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Home controller class
 * This is only viewable to those members that are logged in
 */
 class Erection extends CI_Controller{
    function __construct(){
        parent::__construct();
    }
    
    public function index(){
        if($this->session->userdata('hak_akses') == null) {
            redirect(base_url() . 'login');
        }
        else {
            $this->load->view('home');
        }
    }

    //====================================== Link =================================================================
    
    public function tambaherection($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            $this->load->model('Erectionmodel');
            $data['id'] = $this->Erectionmodel->ambilmaxid()+1;
            $data['hasil'] = $hasil;
            $this->load->view('administrator/erection/tambaherection', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function lihaterection($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            /*$this->load->model('Idenmodel');
            $data['id'] = $this->Idenmodel->ambilmaxid()+1;*/
            $data['hasil'] = $hasil;
            $this->load->view('administrator/erection/lihaterection', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function itperec($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            $data['hasil'] = $hasil;
            $this->load->view('administrator/erection/clerec', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
    }

    public function submititperec(){
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->idproj = $barang->idproj;
            $obj->proses = $barang->proses;
            $obj->nama_item = $barang->nama_item;
            $obj->galangan = $barang->galangan;
            $obj->owner = $barang->owner;
            $obj->cls = $barang->cls;
            $obj->sr = $barang->sr;
            $obj->std = $barang->std;

            array_push($itemlist, $obj);
        }

        $this->load->model('Erectionmodel');
        if($this->Erectionmodel->submititperec($itemlist)){
            $data['hasil'] = "sukses";
        }
        else{
            $data['hasil'] = "gagal";
        }
        $this->load->view('administrator/erection/clerec', $data);
    }

    public function edititperec(){
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->idproj = $barang->idproj;
            $obj->proses = $barang->proses;
            $obj->nama_item = $barang->nama_item;
            $obj->galangan = $barang->galangan;
            $obj->owner = $barang->owner;
            $obj->cls = $barang->cls;
            $obj->sr = $barang->sr;
            $obj->std = $barang->std;
            $obj->status = $barang->status;

            array_push($itemlist, $obj);
        }

        $this->load->model('Erectionmodel');
        if($this->Erectionmodel->edititperec($itemlist)){
            $data['hasil'] = "sukses";
        }
        else{
            $data['hasil'] = "gagal";
        }
        $this->load->view('administrator/erection/listclerec', $data);
    }

    public function suntingitperec($proses){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {
            $data['hasil'] = null;
            $data['proses'] = str_replace('%20', ' ', $proses);
            $this->load->view('administrator/erection/suntingclerec', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
    }

    public function lihatitperec($hasil = null){
        if($this->session->userdata('hak_akses') != null) {
            $data['hasil'] = $hasil;
            $this->load->view('administrator/erection/listclerec', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function suntingerection($hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {  
            $idenparsing = $this->input->post('idenparsing');
            $idproj = $this->input->post('idproj');            
            $data['idenparsing'] = $idenparsing;
            $data['idproj'] = $idproj;            
            $data['hasil'] = $hasil;
            $this->load->view('administrator/erection/suntingerection', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    public function suntingerectionn($idpro, $ider, $hasil = null){
        if($this->session->userdata('hak_akses') == 1 && $this->session->userdata('pilihan_project') != 0) {  
            $idenparsing = $ider;
            $idproj = $idpro;            
            $data['idenparsing'] = $idenparsing;
            $data['idproj'] = $idproj;            
            $data['hasil'] = $hasil;
            $this->load->view('administrator/erection/suntingerection', $data);
        }
        else{
            redirect(base_url() . 'login');    
        }
        
    }

    //======================================================================================================================


    //================================= Submit Identifikasi Material =======================================================


     public function submiterection($hasil = null){

        $idproj = $this->input->post('idproj');
        $id_erection = $this->input->post('id_erection');        
        $proses = $this->input->post('proses');
        $namablok = $this->input->post('namablok');  
        $pros = $this->input->post('pros');
        $alat = $this->input->post('alat');
        $rules = $this->input->post('rules');

        $qcinspec = $this->input->post('qcinspec');
        $qacoor = $this->input->post('qacoor');
        $classsur = $this->input->post('classsur');
        $ownersur = $this->input->post('ownersur');
        $statereg = $this->input->post('statereg');
        $tanggalperiksa = $this->input->post('tanggalperiksa');
        $status = $this->input->post('status');

        date_default_timezone_set("Asia/Jakarta");
        $wktinp = date('Y-m-d H:i:s');
        $oleh = $this->session->userdata('nama_pengguna');
        
        if($status == "OK"){            
            $reinspek = "2001-01-01";
            $rekomendasi = "null";
        }
        else{
            $reinspek = $this->input->post('reinspek');
            $rekomendasi = $this->input->post('rekomendasi');    
        }

        $path = $path2 = $path3 = $path4 = null;

        if($_FILES['gambar1']['name'])
        {
            $filename = $_FILES['gambar1']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path = 'upload/erection/'.$filename;
        }

        if($_FILES['gambar2']['name']){
            $filename = $_FILES['gambar2']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path2 = 'upload/erection/'.$filename;
        }

        if($_FILES['gambar3']['name']){
            $filename = $_FILES['gambar3']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path3 = 'upload/erection/'.$filename;
        }

        if($_FILES['gambar4']['name']){
            $filename = $_FILES['gambar4']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path4 = 'upload/erection/'.$filename;
        }
        
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->nama = $barang->nama;
            $obj->item = $barang->item;
            $obj->standard = $barang->standard;
            $obj->pihak = $barang->pihak;

            array_push($itemlist, $obj);

        }


        $this->load->model('Erectionmodel');

        if($this->Erectionmodel->tambahisierection($idproj, $id_erection, $proses, $namablok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh)){
            if ($this->Erectionmodel->tambahitemerection($id_erection, $itemlist)) {
                
                $ctr = 0;
                    $scan = array();
                    $config['upload_path'] = './upload/erection';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2048000';

                    $this->load->library('upload', $config);

                    for ($i = 1; $i <=4; $i++){
                        if (!empty($_FILES['gambar'.$i]['name'])) {
                            if (!$this->upload->do_upload('gambar'.$i))
                            {
                                echo $this->upload->display_errors();
                                $scan[$i] = array('file_name' => NULL);
                                $ctr++;
                            }
                            else
                            {
                                $scan[$i] = $this->upload->data();
                            }
                        }
                        else{
                            $scan[$i] = array('file_name' => NULL);
                        }

                    }

                if ( $ctr > 0)
                {
                    $error = array('error' => $this->upload->display_errors());
                    echo $this->upload->display_errors();
                    /*$hasil = 'gagal';
                     redirect('idenmat/hullcon/'. $hasil);*/
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                    /*$hasil = 'sukses';
                    redirect('erection/tambaherection/'. $hasil);*/

                    ob_start();
                    require 'fpdf.php';

                    $pdf = new FPDF('P', 'cm', 'A4');
                    $pdf->SetAutoPageBreak(true);
                    $pdf->AddFont('courier', '', 'courier.php');
                    $pdf->AddFont('timesb', '', 'timesb.php');
                    $pdf->AddFont('times', '', 'times.php');
                    $pdf->SetMargins(1.5, 1.5, 1.5, 1.5);
                    $pdf->AddPage('P', 'A4');
                    $pdf->SetFont('timesb', '', 22);

                    //===================== Judul dan PT ==========================================
                    $pdf->Image('./assets/img/PIN.png', 1.5, 1.5,4,2.4);                     

                    $pdf->Cell(22, 0, 'PT. Putra Rahwana Shipyard', 0, 1, "C");
                    $pdf->SetFont('times', '', 16);
                    $pdf->Ln(0.5);
                    $pdf->Cell(22, 1, 'Jalan Gatot Subroto No. 32, Ketapang', 0, 1, "C");
                    $pdf->Ln(0.4);
                    $pdf->Cell(22, 0, 'Banyuwangi, Jawa Timur ', 0, 1, "C");
                    $pdf->Ln(0.1);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.7,24);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.8,24);
                    $pdf->Ln(1.8);
                    $pdf->SetFont('timesb', '', 16);

                    $pdf->Cell(26, 0.4, 'Erection', 0, 1, "L");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(0.7);

                    //=========================== Isi Tahapan ===================================
                    $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
                    $pdf->Ln(0.4);
                    $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    //=========================== Info Umum ====================================
                    $pdf->Cell(4.2, 0.4, 'Tanggal Periksa', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4,  $tanggalperiksa, 0, 1, 'L');
                    $pdf->Ln(0.4);
                    $pdf->Cell(4.2, 0.4, 'Status', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $status, 0, 1, 'L');
                    $pdf->Ln(1);

                    

                    //============================ item ===============================
                    
                    $pdf->SetFont('timesb', '', 12);
                    $header = array('Nama Item', 'Isi Item', 'Standard Item', 'Pihak Pemeriksa');
                    $w = array(7.5, 3.4, 3.4, 3.4);
                    // Header tabel
                    for($i=0;$i<count($header);$i++)
                        $pdf->Cell($w[$i],1,$header[$i],1,0,'C');
                    $pdf->Ln();

                    //isi tabel
                    $pdf->SetFont('times', '', 11);
                    foreach($daftarBarang as $row)
                    {
                        $pdf->Cell($w[0],1,$row->nama,'LR',0,'L');
                        $pdf->Cell($w[1],1,$row->item,'LR',0,'L');
                        $pdf->Cell($w[2],1,$row->standard,'LR',0,'L');            
                        $pdf->Cell($w[3],1,$row->pihak,'LR',0,'L');            
                        $pdf->Ln();
                    }
                    // Closing line
                    $pdf->Cell(array_sum($w),0,'','T');

                    //====================== kalo  Reject ini tempatnya =====================
                    $pdf->Ln(1);  
                    $pdf->SetFont('times', '', 13);
                    if($status == "Reject"){
                        $pdf->Cell(4.2, 0.4, 'Tanggal Reinspeksi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $reinspek, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'Rekomendasi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $rekomendasi, 0, 1, 'L');
                        $pdf->Ln(1.2);
                    }
                    
                    $pdf->SetFont('times', '', 13);

                    //============================== TTD ====================================

                    $pdf->Cell(18, 0.4, 'Yang Memeriksa,', 0, 0, "C");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'QC Inspector', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qcinspec, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'QA Coordinator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qacoor, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Class Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $classsur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Owner Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $ownersur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'State Regulator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $statereg, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'Diinput Oleh', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $oleh, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    date_default_timezone_set("Asia/Jakarta");
                    $pdf->Cell(4.2, 0.4, 'Waktu Input', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $wktinp, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    // $pdf->Cell(5, 0, 'QC Inspector', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'QA Coordinator', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $qcinspec, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $qacoor , 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(5, 0, 'Class Surveyor', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'Owner Surveyor', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $classsur, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $ownersur, 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(18, 0.4, 'State Regulator,', 0, 0, "C");

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(18, 0.4, $statereg, 0, 0, "C");
                    
                    if(!empty($path)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 1', 0, 1, "L");
                        $pdf->Image($path, 1.5, 2, 18, 26);
                    }
                    
                    if(!empty($path2)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 2', 0, 1, "L");
                        $pdf->Image($path2, 1.5, 2, 18, 26);
                    }

                    if(!empty($path3)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 3', 0, 1, "L");
                        $pdf->Image($path3, 1.5, 2, 18, 26);
                    }

                    if(!empty($path4)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 4', 0, 1, "L");
                        $pdf->Image($path4, 1.5, 2, 18, 26);
                    }

                    /*$pdf->AddPage('P', 'A4');
                    $pdf->Image('./'.$path, 2, 2,15,25); */

                    $pdf->Output();
                    ob_end_flush();

                    
                }


            }
            else{
                echo "gagal";
            }
            
        }
        else{
                echo "gagal";
        }   
    }

    //=============================================================================================================================


    //===================================== Update Identifikasi Material ==========================================================

    public function updateisierection($hasil = null){

        $idproj = $this->input->post('idproj');
        $id_erection = $this->input->post('id_erection');
        $proses = $this->input->post('proses');                
        $namablok = $this->input->post('namablok');     
        $pros = $this->input->post('pros');
        $alat = $this->input->post('alat');
        $rules = $this->input->post('rules');

        $qcinspec = $this->input->post('qcinspec');
        $qacoor = $this->input->post('qacoor');
        $classsur = $this->input->post('classsur');
        $ownersur = $this->input->post('ownersur');
        $statereg = $this->input->post('statereg');
        $tanggalperiksa = $this->input->post('tanggalperiksa');
        $status = $this->input->post('status');       

        date_default_timezone_set("Asia/Jakarta");
        $wktinp = date('Y-m-d H:i:s');
        $oleh = $this->session->userdata('nama_pengguna'); 
        
        if($status == "OK"){            
            $reinspek = "2001-01-01";
            $rekomendasi = "null";
        }
        else{
            $reinspek = $this->input->post('reinspek');
            $rekomendasi = $this->input->post('rekomendasi');    
        }

        $path = $path2 = $path3 = $path4 = null;

        if($_FILES['gambar1']['name'])
        {
            $filename = $_FILES['gambar1']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path = 'upload/erection/'.$filename;
        }
        else{
            $path = $this->input->post('imgdulu1');
        }

        if($_FILES['gambar2']['name']){
            $filename = $_FILES['gambar2']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path2 = 'upload/erection/'.$filename;
        }
        else{
            $path2 = $this->input->post('imgdulu2');
        }

        if($_FILES['gambar3']['name']){
            $filename = $_FILES['gambar3']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path3 = 'upload/erection/'.$filename;
        }
        else{
            $path3 = $this->input->post('imgdulu3');
        }

        if($_FILES['gambar4']['name']){
            $filename = $_FILES['gambar4']['name'];
            $filename = preg_replace('/\s/', '_', $filename);
            $path4 = 'upload/erection/'.$filename;
        }
        else{
            $path4 = $this->input->post('imgdulu4');
        }
        
        $daftarBarang = json_decode($this->input->post('daftarBarang'));
        $itemlist = [];
        foreach ($daftarBarang as $barang) {
            $obj = new stdClass();
            $obj->nama = $barang->nama;
            $obj->item = $barang->item;
            $obj->standard = $barang->standard;
            $obj->pihak = $barang->pihak;

            array_push($itemlist, $obj);

        }


        $this->load->model('Erectionmodel');

        if($this->Erectionmodel->suntingisierection($idproj, $id_erection, $namablok, $pros, $alat, $rules, $qcinspec, $qacoor, $classsur, $ownersur, $statereg, $tanggalperiksa, $status, $reinspek, $rekomendasi, $path, $path2, $path3, $path4, $wktinp, $oleh)){
            if ($this->Erectionmodel->updateitemerection($id_erection, $itemlist)) {
                $ctr = 0;
                    $scan = array();
                    $config['upload_path'] = './upload/erection';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '2048000';

                    $this->load->library('upload', $config);

                    for ($i = 1; $i <=4; $i++){
                        if (!empty($_FILES['gambar'.$i]['name'])) {
                            if (!$this->upload->do_upload('gambar'.$i))
                            {
                                echo $this->upload->display_errors();
                                $scan[$i] = array('file_name' => NULL);
                                $ctr++;
                            }
                            else
                            {
                                $scan[$i] = $this->upload->data();
                            }
                        }
                        else{
                            $scan[$i] = array('file_name' => NULL);
                        }

                    }

                if ( $ctr > 0)
                {
                    $error = array('error' => $this->upload->display_errors());
                    echo $this->upload->display_errors();
                    
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                    /*$hasil = 'sukses';
                    redirect('erection/lihaterection/'. $hasil);*/

                    ob_start();
                    require 'fpdf.php';

                    $pdf = new FPDF('P', 'cm', 'A4');
                    $pdf->SetAutoPageBreak(true);
                    $pdf->AddFont('courier', '', 'courier.php');
                    $pdf->AddFont('timesb', '', 'timesb.php');
                    $pdf->AddFont('times', '', 'times.php');
                    $pdf->SetMargins(1.5, 1.5, 1.5, 1.5);
                    $pdf->AddPage('P', 'A4');
                    $pdf->SetFont('timesb', '', 22);

                    //===================== Judul dan PT ==========================================
                    $pdf->Image('./assets/img/PIN.png', 1.5, 1.5,4,2.4);                     

                    $pdf->Cell(22, 0, 'PT. Putra Rahwana Shipyard', 0, 1, "C");
                    $pdf->SetFont('times', '', 16);
                    $pdf->Ln(0.5);
                    $pdf->Cell(22, 1, 'Jalan Gatot Subroto No. 32, Ketapang', 0, 1, "C");
                    $pdf->Ln(0.4);
                    $pdf->Cell(22, 0, 'Banyuwangi, Jawa Timur ', 0, 1, "C");
                    $pdf->Ln(0.1);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.7,24);
                    $pdf->Image('./assets/img/pembatasn.png', 0, 4.8,24);
                    $pdf->Ln(1.8);
                    $pdf->SetFont('timesb', '', 16);

                    $pdf->Cell(26, 0.4, 'Erection', 0, 1, "L");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(0.7);

                    //=========================== Isi Tahapan ===================================
                    $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
                    $pdf->Ln(0.4);
                    $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    //=========================== Info Umum ====================================
                    $pdf->Cell(4.2, 0.4, 'Tanggal Periksa', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4,  $tanggalperiksa, 0, 1, 'L');
                    $pdf->Ln(0.4);
                    $pdf->Cell(4.2, 0.4, 'Status', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $status, 0, 1, 'L');
                    $pdf->Ln(1);

                    

                    //============================ item ===============================
                    
                    $pdf->SetFont('timesb', '', 12);
                    $header = array('Nama Item', 'Isi Item', 'Standard Item', 'Pihak Pemeriksa');
                    $w = array(7.5, 3.4, 3.4, 3.4);
                    // Header tabel
                    for($i=0;$i<count($header);$i++)
                        $pdf->Cell($w[$i],1,$header[$i],1,0,'C');
                    $pdf->Ln();

                    //isi tabel
                    $pdf->SetFont('times', '', 11);
                    foreach($daftarBarang as $row)
                    {
                        $pdf->Cell($w[0],1,$row->nama,'LR',0,'L');
                        $pdf->Cell($w[1],1,$row->item,'LR',0,'L');
                        $pdf->Cell($w[2],1,$row->standard,'LR',0,'L');            
                        $pdf->Cell($w[3],1,$row->pihak,'LR',0,'L');            
                        $pdf->Ln();
                    }
                    // Closing line
                    $pdf->Cell(array_sum($w),0,'','T');

                    //====================== kalo  Reject ini tempatnya =====================
                    $pdf->Ln(1);  
                    $pdf->SetFont('times', '', 13);
                    if($status == "Reject"){
                        $pdf->Cell(4.2, 0.4, 'Tanggal Reinspeksi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $reinspek, 0, 1, 'L');
                        $pdf->Ln(0.4);
                        $pdf->Cell(4.2, 0.4, 'Rekomendasi', 0, 0, "L");
                        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                        $pdf->Cell(3, 0.4, $rekomendasi, 0, 1, 'L');
                        $pdf->Ln(1.2);
                    }
                    
                    $pdf->SetFont('times', '', 13);

                    //============================== TTD ====================================

                    $pdf->Cell(18, 0.4, 'Yang Memeriksa,', 0, 0, "C");
                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'QC Inspector', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qcinspec, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'QA Coordinator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $qacoor, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Class Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $classsur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'Owner Surveyor', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $ownersur, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->Cell(4.2, 0.4, 'State Regulator', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $statereg, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    $pdf->SetFont('times', '', 13);
                    $pdf->Ln(1.0);

                    $pdf->Cell(4.2, 0.4, 'Diinput Oleh', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $oleh, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    date_default_timezone_set("Asia/Jakarta");
                    $pdf->Cell(4.2, 0.4, 'Waktu Input', 0, 0, "L");
                    $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
                    $pdf->Cell(3, 0.4, $wktinp, 0, 1, 'L');
                    $pdf->Ln(0.4);

                    // $pdf->Cell(5, 0, 'QC Inspector', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'QA Coordinator', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $qcinspec, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $qacoor , 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(5, 0, 'Class Surveyor', 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, 'Owner Surveyor', 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(5, 0, $classsur, 0, 0,'C');
                    // $pdf->Cell(2, 0, '', 0, 0, 'R');
                    // $pdf->Cell(15, 0, $ownersur, 0, 0, 'C');

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('timesb', '', 13);
                    // $pdf->Cell(18, 0.4, 'State Regulator,', 0, 0, "C");

                    // $pdf->Ln(2.5);

                    // $pdf->SetFont('times', '', 13);
                    // $pdf->Cell(18, 0.4, $statereg, 0, 0, "C");
                    
                    if(!empty($path)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 1', 0, 1, "L");
                        $pdf->Image($path, 1.5, 2, 18, 26);
                    }
                    
                    if(!empty($path2)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 2', 0, 1, "L");
                        $pdf->Image($path2, 1.5, 2, 18, 26);
                    }

                    if(!empty($path3)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 3', 0, 1, "L");
                        $pdf->Image($path3, 1.5, 2, 18, 26);
                    }

                    if(!empty($path4)){
                        $pdf->AddPage('P', 'A4');            
                        $pdf->Cell(26, 0.4, 'Lampiran 4', 0, 1, "L");
                        $pdf->Image($path4, 1.5, 2, 18, 26);
                    }

                    /*$pdf->AddPage('P', 'A4');
                    $pdf->Image('./'.$path, 2, 2,15,25); */

                    $pdf->Output();
                    ob_end_flush();

                }


            }
            else{
                echo "gagal";
            }
            
        }
        else{
                echo "gagal";
        }

    }

    public function cetakerection(){

        
        $idproj = $this->input->post('idproj');
        $idenmat = $this->input->post('idenparsing');

        $this->load->model('Erectionmodel');
        $query = $this->Erectionmodel->ambildetailerection($idproj, $idenmat);
        
        $jsonstr = json_encode($query->result_array());  

        foreach ($query->result() as $row)
        {
              

            $idproj = $row->id_project;             
            $proses = $row->proses_erection;
            $namablok = $row->blok_erection;            

            $qcinspec = $row->qc_inspecer;
            $qacoor = $row->qa_coorer;
            $classsur = $row->class_surrer;
            $ownersur = $row->owner_surer;
            $statereg = $row->state_reger;
            $tanggalperiksa = $row->tgl_periksaer;
            $status = $row->status_erection;
            $reinspek = $row->tgl_reinspeksier;
            $rekomendasi = $row->rekomendasi_erection;
            $path = $row->path_gambarer;
            $path2 = $row->path_gambarer2;
            $path3 = $row->path_gambarer3;
            $path4 = $row->path_gambarer4;

            $wktinp = $row->input_er;
            $oleh = $row->oleh_er;

        }

         $querys = $this->Erectionmodel->ambilulangitemer($idenmat);

         ob_start();
        require 'fpdf.php';

        $pdf = new FPDF('P', 'cm', 'A4');
        $pdf->SetAutoPageBreak(true);
        $pdf->AddFont('courier', '', 'courier.php');
        $pdf->AddFont('timesb', '', 'timesb.php');
        $pdf->AddFont('times', '', 'times.php');
        $pdf->SetMargins(1.5, 1.5, 1.5, 1.5);
        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('timesb', '', 22);

        //===================== Judul dan PT ==========================================
        $pdf->Image('./assets/img/PIN.png', 1.5, 1.5,4,2.4);                     

        $pdf->Cell(22, 0, 'PT. Putra Rahwana Shipyard', 0, 1, "C");
        $pdf->SetFont('times', '', 16);
        $pdf->Ln(0.5);
        $pdf->Cell(22, 1, 'Jalan Gatot Subroto No. 32, Ketapang', 0, 1, "C");
        $pdf->Ln(0.4);
        $pdf->Cell(22, 0, 'Banyuwangi, Jawa Timur ', 0, 1, "C");
        $pdf->Ln(0.1);
        $pdf->Image('./assets/img/pembatasn.png', 0, 4.7,24);
        $pdf->Image('./assets/img/pembatasn.png', 0, 4.8,24);
        $pdf->Ln(1.8);
        $pdf->SetFont('timesb', '', 16);

        $pdf->Cell(26, 0.4, 'Erection', 0, 1, "L");
        $pdf->SetFont('times', '', 13);
        $pdf->Ln(0.7);

        //=========================== Isi Tahapan ===================================
        $pdf->Cell(4.2, 0.4, 'Nama Proses', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $proses, 0, 1, 'L');
        $pdf->Ln(0.4);
        $pdf->Cell(4.2, 0.4, 'Nama Blok', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $namablok, 0, 1, 'L');
        $pdf->Ln(0.4);

        //=========================== Info Umum ====================================
        $pdf->Cell(4.2, 0.4, 'Tanggal Periksa', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4,  $tanggalperiksa, 0, 1, 'L');
        $pdf->Ln(0.4);
        $pdf->Cell(4.2, 0.4, 'Status', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $status, 0, 1, 'L');
        $pdf->Ln(1);

        

        //============================ item ===============================
        
        $pdf->SetFont('timesb', '', 12);
        $header = array('Nama Item', 'Isi Item', 'Standard Item', 'Pihak Pemeriksa');
        $w = array(7.5, 3.4, 3.4, 3.4);
        // Header tabel
        for($i=0;$i<count($header);$i++)
            $pdf->Cell($w[$i],1,$header[$i],1,0,'C');
        $pdf->Ln();

        //isi tabel
        $pdf->SetFont('times', '', 11);
        foreach ($querys->result() as $rows)
        {
            $pdf->Cell($w[0],1,$rows->nama_itemer,'LR',0,'L');
            $pdf->Cell($w[1],1,$rows->isi_itemer,'LR',0,'L');
            $pdf->Cell($w[2],1,$rows->standard_itemer,'LR',0,'L');            
            $pdf->Cell($w[3],1,$rows->pihak_itemer,'LR',0,'L');            
            $pdf->Ln();
        }
        // Closing line
        $pdf->Cell(array_sum($w),0,'','T');

        //====================== kalo  Reject ini tempatnya =====================
        $pdf->Ln(1);  
        $pdf->SetFont('times', '', 13);
        if($status == "Reject"){
            $pdf->Cell(4.2, 0.4, 'Tanggal Reinspeksi', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $reinspek, 0, 1, 'L');
            $pdf->Ln(0.4);
            $pdf->Cell(4.2, 0.4, 'Rekomendasi', 0, 0, "L");
            $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
            $pdf->Cell(3, 0.4, $rekomendasi, 0, 1, 'L');
            $pdf->Ln(1.2);
        }
        
        $pdf->SetFont('times', '', 13);

        //============================== TTD ====================================

        $pdf->Cell(18, 0.4, 'Yang Memeriksa,', 0, 0, "C");
        $pdf->SetFont('times', '', 13);
        $pdf->Ln(1.0);

        $pdf->Cell(4.2, 0.4, 'QC Inspector', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $qcinspec, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'QA Coordinator', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $qacoor, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'Class Surveyor', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $classsur, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'Owner Surveyor', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $ownersur, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->Cell(4.2, 0.4, 'State Regulator', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $statereg, 0, 1, 'L');
        $pdf->Ln(0.4);

        $pdf->SetFont('times', '', 13);
        $pdf->Ln(1.0);

        $pdf->Cell(4.2, 0.4, 'Diinput Oleh', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $oleh, 0, 1, 'L');
        $pdf->Ln(0.4);

        date_default_timezone_set("Asia/Jakarta");
        $pdf->Cell(4.2, 0.4, 'Waktu Input', 0, 0, "L");
        $pdf->Cell(0.5, 0.4, ':', 0, 0, 'L');
        $pdf->Cell(3, 0.4, $wktinp, 0, 1, 'L');
        $pdf->Ln(0.4);

        // $pdf->Cell(5, 0, 'QC Inspector', 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, 'QA Coordinator', 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('times', '', 13);
        // $pdf->Cell(5, 0, $qcinspec, 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, $qacoor , 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('timesb', '', 13);
        // $pdf->Cell(5, 0, 'Class Surveyor', 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, 'Owner Surveyor', 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('times', '', 13);
        // $pdf->Cell(5, 0, $classsur, 0, 0,'C');
        // $pdf->Cell(2, 0, '', 0, 0, 'R');
        // $pdf->Cell(15, 0, $ownersur, 0, 0, 'C');

        // $pdf->Ln(2.5);

        // $pdf->SetFont('timesb', '', 13);
        // $pdf->Cell(18, 0.4, 'State Regulator,', 0, 0, "C");

        // $pdf->Ln(2.5);

        // $pdf->SetFont('times', '', 13);
        // $pdf->Cell(18, 0.4, $statereg, 0, 0, "C");

        if(!empty($path)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 1', 0, 1, "L");
            $pdf->Image($path, 1.5, 2, 18, 26);
        }
        
        if(!empty($path2)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 2', 0, 1, "L");
            $pdf->Image($path2, 1.5, 2, 18, 26);
        }

        if(!empty($path3)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 3', 0, 1, "L");
            $pdf->Image($path3, 1.5, 2, 18, 26);
        }

        if(!empty($path4)){
            $pdf->AddPage('P', 'A4');            
            $pdf->Cell(26, 0.4, 'Lampiran 4', 0, 1, "L");
            $pdf->Image($path4, 1.5, 2, 18, 26);
        }

        /*$pdf->AddPage('P', 'A4');
        $pdf->Image('./'.$path, 2, 2,15,25); */

        $pdf->Output();
        ob_end_flush();
     }


 }